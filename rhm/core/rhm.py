import itertools
import numpy as np
import collections
import sys
import gc
import copy
from copy import deepcopy
import logging
from rhm.core import GeneralizedPattern as Gp
from rhm.core import PatternStatistics as Ps
from rhm.core import RHMDataHandler as Rh
import rhm.core.Discretizer as Ed
import rhm.learningset.LearningSetBuilder as Tsls
from rhm.core import RHMDataHolder as Rdh


logger = logging.getLogger(__name__)


class RHM(object):

    def __init__(self, config):
        self._all_variables = []
        self._names = []
        self._train_data_len = 0
        self._label_cov_dict = dict()
        self._config = config
        self._valid_patterns = dict()
        self._black_patterns = dict()
        self._visu_raw = dict()
        self._visu_disc = dict()
        self._visu_pattern = dict()
        self._visu_raw_metadata = dict()
        self._visu_disc_metadata = dict()

    @property
    def config(self):
        return self._config

    @property
    def labels_coverage(self):
        return self._label_cov_dict

    @property
    def all_variables(self):
        return self._all_variables

    @property
    def names(self):
        return self._names

    @names.setter
    def names(self, value):
        self._names = value

    @property
    def train_data_len(self):
        return self._train_data_len

    @train_data_len.setter
    def train_data_len(self, value):
        self._train_data_len = value

    @property
    def valid_patterns(self):
        return self._valid_patterns

    @valid_patterns.setter
    def valid_patterns(self, value):
        self._valid_patterns = value

    @property
    def black_patterns(self):
        return self._black_patterns

    @black_patterns.setter
    def black_patterns(self, value):
        self._black_patterns = value

    @property
    def visu_raw(self):
        return self._visu_raw

    @visu_raw.setter
    def visu_raw(self, value):
        self._visu_raw = value

    @property
    def visu_disc(self):
        return self._visu_disc

    @visu_disc.setter
    def visu_disc(self, value):
        self._visu_disc = value

    @property
    def visu_pattern(self):
        return self._visu_pattern

    @visu_pattern.setter
    def visu_pattern(self, value):
        self._visu_pattern = value

    @property
    def visu_raw_metadata(self):
        return self._visu_raw_metadata

    @visu_raw_metadata.setter
    def visu_raw_metadata(self, value):
        self._visu_raw_metadata = value

    @property
    def visu_disc_metadata(self):
        return self._visu_disc_metadata

    @visu_disc_metadata.setter
    def visu_disc_metadata(self, value):
        self._visu_disc_metadata = value

    # Expanding (best one-step)
    def expand_ranges(self, current_pattern, upper_bound, windows_list, rhm_dh, pos_dict):

        """
        expanding each pattern by one step up and one step down on a sequential matter (position).
        e.g. pattern [BC]B will start by expanding [BC], thus generating
        [AC]B,[BD]B and [AD]B to be checked while keeping the best one (most general), for example [BD]B.
        Then we generate [BD][AB], [BD][BC], [BD][AC] and we keep the best one again.

        :param current_pattern: the pattern to generalize
        :type current_pattern: `GeneralizePattern`
        :param upper_bound: the upper_bound (inclusive) of the generalization
        :type upper_bound: list
        :param windows_list: list of integer of windows size needed in case we are dealing with a pair of variables
        :type windows_list: list of integer
        :param rhm_dh: store information related to rhm parameters and current state
        :type: rhm_dh: RHMDataHolder
        :param pos_dict: A dictionary of dictionary (Letter/bin --> position --> {patterns})
        :type pos_dict: dictionary
        :return: a new pattern
        :rtype:  `GeneralizePattern`
        """

        int_lower_bound = 0
        index = 0
        _range = 0

        if windows_list is None:
            windows_list = [len(current_pattern.p_min)]

        temp_pattern = copy.deepcopy(current_pattern)

        for window in windows_list:
            for pos in range(_range, _range + window):
                int_upper_bound = Rh.to_int(upper_bound[index])
                best_range = 0

                # Generate the list of candidates from the one step expanding method
                for shift in self.one_step_expand(current_pattern, pos, int_lower_bound, int_upper_bound):
                    # Building the new candidate
                    temp_min = current_pattern.p_min[0:pos] + Rh.to_chr(shift[0]) + current_pattern.p_min[pos + 1:]
                    temp_max = current_pattern.p_max[0:pos] + Rh.to_chr(shift[1]) + current_pattern.p_max[pos + 1:]
                    new = Gp.GeneralizedPattern(temp_min, temp_max)

                    pos_keys = self.pattern_to_pos_keys(new)
                    post_dict_window = pos_dict[len(new)]
                    cons_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]

                    # Get all patterns that fall in the area of the pattern 'new'
                    permutation = set.union(*cons_pats)
                    for i in range(1, len(pos_keys)):
                        cons_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
                        cons_pats = set.union(*cons_pats)
                        permutation = permutation.intersection(cons_pats)

                    if rhm_dh.noise_p == 1:
                        # Not_black = new not in black_list
                        for pat in permutation:
                            if pat in rhm_dh.black_set:
                                not_black = False
                                break
                        else:
                            not_black = True
                    else:
                        not_black = self.noise_handling_generalizing(rhm_dh, permutation)

                    _shift_ar = shift[1] - shift[0]
                    if not_black and best_range < _shift_ar:
                        best_range = _shift_ar
                        temp_pattern = new

                current_pattern = temp_pattern

            index += 1
            _range += window

        return current_pattern

    def one_step_expand(self, pattern, pos, lower, upper):

        """
        Generate the set of one-step extended patterns depending for a specific position
        e.g. [B] will give [AB], [BC], [AC]

        :param pattern: original pattern
        :param pos: position to extended in the pattern
        :param lower: lower range bin
        :param upper: upper range bin
        :return: list of candidates patterns
        """
        expands = []
        pos_min = pattern.p_min[pos]
        pos_min_int = Rh.to_int(pos_min)
        pos_max = pattern.p_max[pos]
        pos_max_int = Rh.to_int(pos_max)

        if pos_min_int == lower:
            down_range = 0
        else:
            down_range = pos_min_int - 1
        if pos_max_int == upper:
            up_range = upper
        else:
            up_range = pos_max_int + 1
        for shift in itertools.product(range(down_range, pos_min_int + 1), range(pos_max_int, up_range + 1)):
            expands.append(shift)
        return expands

    # Expanding (all one step up/down)
    def expand_patterns(self, pattern_set, curr_var, rhm_dh, pos_dict):

        """
        expanding each pattern by one step up and one step down.
        e.g. pattern [BC]B will generate:
        { [AC]B, [BD]B, [AD]B, [AC][AB], [BD][AB], [AD][AB], [AC][BC], [BD][BC], [AD][BC],
        [AC][AC], [BD][AC], [AD][AC], [BC][AB], [BC][BC], [BC][AC]} and keep the ones that satisfy the noise condition.

        :param pattern_set: Patterns to be expanded
        :type pattern_set: set of `GeneralizePattern`
        :param curr_var: the current variable, this is necessary in case we have patterns for pair of variables.
        :type curr_var: list of integer (variable index)
        :param rhm_dh: store information related to rhm parameters and current state
        :type: rhm_dh: RHMDataHolder
        :param pos_dict: A dictionary of dictionary (Letter/bin --> position --> {patterns})
        :type pos_dict: dictionary
        :return: a new pattern
        :rtype:  list of `GeneralizePattern`
        """

        lower_bound = 'A'
        lower = Rh.to_int(lower_bound)
        new_candidates = set()
        checked_pattern = set()
        for pat in sorted(pattern_set):
            checked_pattern.add(pat)
            g_pat = self.strpat_to_pattern(pat)
            if len(curr_var) > 1:
                windows_list = len(g_pat.p_min) / 2
            else:
                windows_list = len(g_pat.p_min)
            candidate_list = self.expand_one_step(g_pat, windows_list, rhm_dh.bins, lower, curr_var)

            for candidate_pat in candidate_list:
                if candidate_pat in checked_pattern:
                    continue
                else:
                    checked_pattern.add(candidate_pat)
                new = self.strpat_to_pattern(candidate_pat)

                pos_keys = self.pattern_to_pos_keys(new)
                post_dict_window = pos_dict[len(new)]
                cons_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
                permutation = set.union(*cons_pats)
                for i in range(1, len(pos_keys)):
                    cons_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
                    cons_pats = set.union(*cons_pats)
                    permutation = permutation.intersection(cons_pats)

                if int(rhm_dh.noise_p) == 1:
                    for _pat in permutation:
                        if _pat in rhm_dh.black_set:
                            not_black = False
                            break
                    else:
                        not_black = True
                else:
                    not_black = self.noise_handling_generalizing(rhm_dh, permutation)

                if not_black:
                    new_candidates.add(candidate_pat)

        return new_candidates

    def expand_one_step(self, pattern, windows_size, upper_bound, lower_bound, curr_var):
        candidates = []
        gc.enable()
        for pos in range(len(pattern)):
            if pos >= windows_size:
                temp_candidate = self.one_step_expand_str(pattern, pos, lower_bound, upper_bound[curr_var[1]] - 1)
            else:
                temp_candidate = self.one_step_expand_str(pattern, pos, lower_bound, upper_bound[curr_var[0]] - 1)
            if len(candidates) == 0:
                candidates = copy.deepcopy(temp_candidate)
            else:
                concat = []
                for pat_cn in candidates:
                    for pat_tmp in temp_candidate:
                        concat.append(pat_cn + pat_tmp)
                candidates = concat
            gc.collect()
        return candidates

    def one_step_expand_str(self, pattern, pos, lower, upper):

        """
        Generate the set of one-step extended patterns depending for a specific position
        :param pattern: original pattern
        :param pos: position to extended in the pattern
        :param lower: lower range bin
        :param upper: upper range bin
        :return: list of candidates patterns
        """

        expands = []
        pos_min = pattern.p_min[pos]
        pos_min_int = Rh.to_int(pos_min)
        pos_max = pattern.p_max[pos]
        pos_max_int = Rh.to_int(pos_max)

        if pos_min_int == lower:
            down_range = 0
        else:
            down_range = pos_min_int - 1
        if pos_max_int == upper:
            up_range = upper
        else:
            up_range = pos_max_int + 1
        for shift in itertools.product(range(down_range, pos_min_int + 1), range(pos_max_int, up_range + 1)):
            if shift[0] == shift[1]:
                expands.append(Rh.to_chr(shift[0]))
            else:
                expands.append('[' + Rh.to_chr(shift[0]) + Rh.to_chr(shift[1]) + ']')
        return expands

    # Patterns manipulation
    def generalize(self, one, other):

        """
        Generalizes two patterns to a new one
        :param one: a given pattern
        :param one: GeneralizedPattern
        :param other: a given pattern.
        :type other: GeneralizedPattern
        :return: a new generalized pattern
        :rtype: GeneralizedPattern

        Generalization is done like this:
        * patterns with the same length are generalized
        * patterns [AF][CD][CK] and [BE][BG][DE] are generalized as [AF][BG][CK]
        """

        new_stats = Ps.PatternStatistics()
        new_stats.merge(copy.deepcopy(one.stats))
        new_stats.black_merge(copy.deepcopy(one.stats))
        new_stats.merge(copy.deepcopy(other.stats))
        new_stats.black_merge(copy.deepcopy(other.stats))

        if one.p_max is None:
            # both None
            if other.p_max is None:
                pat_min = [min(_o, _a) for (_o, _a) in zip(one.p_min, other.p_min)]
                pat_max = [max(_o, _a) for (_o, _a) in zip(one.p_min, other.p_min)]
                ret = Gp.GeneralizedPattern(''.join(pat_min), ''.join(pat_max), stats=new_stats)
            else:
                # one max is None other is not
                pat_min = [min(_o[0], _a[0]) for (_o, _a) in zip(one.p_min, other.p_min)]
                pat_max = [max(_o, _a) for (_o, _a) in zip(one.p_min, other.p_max)]
                ret = Gp.GeneralizedPattern(''.join(pat_min), ''.join(pat_max), stats=new_stats)
        else:
            # one max is not None and other is None
            if other.p_max is None:
                pat_min = [min(_o[0], _a[0]) for (_o, _a) in zip(one.p_min, other.p_min)]
                pat_max = [max(_o[0], _a[0]) for (_o, _a) in zip(one.p_max, other.p_min)]
                ret = Gp.GeneralizedPattern(''.join(pat_min), ''.join(pat_max), stats=new_stats)
            else:
                # none are None
                pat = [(min(_o[0], _a[0]), max(_o[1], _a[1])) for (_o, _a) in zip(one, other)]
                ret = Gp.GeneralizedPattern(''.join(x[0] for x in pat), ''.join(x[1] for x in pat), stats=new_stats)
        return ret

    # Bag of patterns
    def pattern_generator_multivar(self, windows_list, symbols, variables, no_gap=True, gap=0):

        """
        Returns a list of list, where each contains the patterns related
        to a different window size (window_list) for a pair of variables.
        (currently only work with no gap)
        :param windows_list: a list of window sizes
        :param symbols: a string representation of the timeseries
        :param variables: the pair of variables
        :param no_gap: whether there is a gap between the values of the two variables or not.
        :param gap: how much gap is there between the two variables
        :return: a list of list (string) that will enumerate all the patterns.
        """

        if not no_gap:
            for i in range(len(variables)):
                symbols[variables[i]] = symbols[variables[i]][sum(windows_list[0:i]) + gap - 1:]

            nb_windows = len(symbols[len(variables) - 1])
            var = len(windows_list) - 1

        else:
            nb_windows = len(symbols[variables[0]])
            var = 0

        all_patterns = []
        for i in range(nb_windows - windows_list[var] + 1):
            _str = ''
            for j in range(len(windows_list)):
                _str += "".join([x for x in symbols[variables[j]][i:i + windows_list[j]]])
            all_patterns.append(_str)
        return all_patterns

    def compute_stats_multivar(self, learning_set, window_list, no_gap=True, gap=0):

        """
        Computes the pattern statistics of a learning set
        :param learning_set: the learning set
        :type learning_set: LearningSet
        :param window_list: a list of window sizes
        :type window_list: list of integer
        :param no_gap: whether there is a gap between the values of the two variables or not.
        :param gap: how much gap is there between the two variables

        :return: PatterStatistic for all the learned patterns.
        :rtype:  PatternStatistic
        """

        _all_labels = set(learning_set.get_labels())
        _all_stats = dict()
        nb_vars = learning_set[0].nb_variables()

        for label in _all_labels:
            for i,j in itertools.combinations(range(nb_vars), 2):
                _temp_str = str(i) + '-' + str(j)
                _all_stats[label + ':' + _temp_str] = dict()

        for (_idx, _ts) in enumerate(learning_set):
            _label = learning_set.get_label(_idx)
            data_symbols = _ts.data_symbols
            for i,j in itertools.combinations(range(nb_vars), 2):
                _temp_str = str(i) + '-' + str(j)
                _hash = _all_stats[_label + ':' + _temp_str]
                _set = set()
                _vars = [i, j]
                _gen = self.pattern_generator_multivar(window_list, data_symbols, _vars)
                for (pos, _pattern) in enumerate(_gen):
                    if _pattern not in _set:
                        _set.add(_pattern)
                        if _pattern not in _hash:
                            _hash[_pattern] = Ps.PatternStatistics()
                        _hash[_pattern].new_example(_ts.id)
                    _hash[_pattern].appears(_ts.id, pos)
        return _all_stats

    def pattern_generator(self, arr, variable_id, window_list):

        """
        Returns a list of list, where each contains the patterns related
        to a different window size (window_list)
        :param arr: a string representation of the timeseries
        :param variable_id
        :param window_list: a list of window sizes
        :return: a list of list (string) that will enumerate all the patterns.
        """

        results = []
        for window in window_list:
            result = []
            for i in range(len(arr[variable_id]) - window + 1):
                result.append("".join([x for x in arr[variable_id][i:i+window]]))
            results.append(result)
        return results

    def compute_stats(self, learning_set, window_list):

        """
        Computes the pattern statistics of a learning set

        :param learning_set: the learning set
        :type learning_set: LearningSet
        :param window_list: a list of window sizes
        :type window_list: list of integer
        :return: PatterStatistic for all the learned patterns.
        :rtype:  PatternStatistic
        """

        # _all_stats['label:var'] is a hash of patterns for label and var
        _nb_variables = learning_set[0].nb_variables()
        _all_labels = set(learning_set.get_labels())
        _all_stats = dict()

        # Initialize the dictionary
        for label in _all_labels:
            for _var_id in np.arange(_nb_variables):
                _all_stats[label + ':' + str(_var_id)] = dict()

        for (_idx, _ts) in enumerate(learning_set):
            _label = learning_set.get_label(_idx)
            _data_symbols = _ts.data_symbols
            for _var_id in np.arange(_nb_variables):
                _set = set()
                _hash = _all_stats[_label + ':' + str(_var_id)]
                _gen = self.pattern_generator(_data_symbols, _var_id, window_list)
                for _pat in _gen:
                    for pos, _pattern in enumerate(_pat):
                        if _pattern not in _set:
                            _set.add(_pattern)
                            if _pattern not in _hash:
                                _hash[_pattern] = Ps.PatternStatistics()
                            _hash[_pattern].new_example(_ts.id)
                        _hash[_pattern].appears(_ts.id, pos)
        return _all_stats

    # Tiling
    def greedy_tiling(self, rhm_dh):

        """
        Does a greedy tiling with pattern_set. We want to tile the
        set of covered example with the minimum number of patterns

        :param rhm_dh: store information related to rhm parameters and current state
        :type: rhm_dh: RHMDataHolder
        :return: the set of selected patterns (as indices), and the tiling set (as a set)
        :rtype: a tuple (array of indices, set)
        """

        res = set()
        best_set = set()
        remains = sorted(set(rhm_dh.valid_set.keys()))
        tiling = set()

        while len(remains) > 0:
            best_set.clear()
            best_key = None
            # looking for the pattern with the covering set that has the largest
            # number of elements not in tiling
            for key in remains:
                current_set = rhm_dh.valid_set[key].stats.example_set
                # len of current set is smaller than the current best -> cannot improve
                if len(current_set) < len(best_set):
                    continue
                if best_key is not None  \
                        and len(key) < len(best_key) \
                        and len(current_set.difference(tiling)) == len(best_set):
                    best_set = current_set.difference(tiling)
                    best_key = key

                temp_set = current_set.difference(tiling)
                if len(temp_set) > len(best_set):
                    best_set = temp_set
                    best_key = key

            if best_key is not None:
                remains.remove(best_key)
                tiling.update(best_set)
                res.add(best_key)
            else:
                # nobody in remains contains element not in tiling -> stop
                break

        """
        #Extension on the greedy tiling:
        #keep all the patterns that cover a certain percentage of examples
        for key in rhm_dh.valid_set:
            if len(rhm_dh.valid_set[key].stats.example_set) >= rhm_dh.conf_threshold:
                res.add(key)
        """
        return res

    def tiling(self, rhm_dh, data_length):

        """
        A different approach for the tiling step, choose the set of patterns based on how
        much weight they have on the timeseries.
        :param rhm_dh: store information related to rhm parameters and current state
        :type: rhm_dh: RHMDataHolder
        :param data_length
        :type data_length: int
        :return:
        """

        res = set()
        remains = copy.deepcopy(set(rhm_dh.valid_set.keys()))
        covered_set = set()
        best_pat = None
        best_set = set()
        for pat in sorted(rhm_dh.valid_set):
            pat_set = rhm_dh.valid_set[pat].stats.example_set
            covered_set.update(pat_set)
            if len(pat_set):
                best_pat = pat
                best_set = rhm_dh.valid_set[best_pat].stats.example_set

        tiling_weight = [1] * data_length
        for ex in best_set:
            tiling_weight[ex] += 1
        covered_set = covered_set.difference(best_set)
        remains.remove(best_pat)
        res.add(best_pat)

        while len(covered_set) > 0:
            max_val = -10000
            best_set = set()
            for pat in sorted(remains):
                best_val = 0
                pat_set = rhm_dh.valid_set[pat].stats.example_set
                black_set = rhm_dh.valid_set[pat].stats.example_bset
                for ex in pat_set:
                    best_val += 1 / tiling_weight[ex]
                for ex in black_set:
                    best_val -= 1 / tiling_weight[ex]
                if best_val > max_val or (best_val == max_val and len(pat_set) > len(best_set)):
                    max_val = best_val
                    best_pat = pat
                    best_set = pat_set
            covered_set = covered_set.difference(best_set)
            remains.remove(best_pat)
            res.add(best_pat)

        # Extension on the greedy tiling:
        # keep all the patterns that cover a certain percentage of examples
        """
        for key in rhm_dh.valid_set:
            if len(rhm_dh.valid_set[key].stats.example_set) >= rhm_dh.conf_threshold:
                res.add(key)
        """
        return res

    # Noise handling
    def noise_handling_pregeneralize(self, valid_dict, black_dict, rhm_dh):

        """
        Generate the list of valid patterns and the list of black_list patterns (invalid)
        while handling noise (in term of perentage)
        This method assume that the patterns are in their most specific form [no generalization applied]

        :param valid_dict: Positive patterns
        :type valid_dict: default dictionary of PatternStatistic
        :param black_dict: Negative patterns
        :type black_dict: default dictionary of PatternStatistic
        :param rhm_dh: data holder, hold the noise, threshold, ...
        :type rhm_dh: RHMDataHolder
        """

        keys_a = valid_dict.keys()
        keys_b = black_dict.keys()

        # Find the ones that occur in both groups
        intersection = sorted(keys_a & keys_b)
        valid = set()

        # Check the noise percentage in term of example coverage (satify also unbalanced dataset)
        for key in intersection:
            _statistic_0 = valid_dict[key]
            _count_0 = _statistic_0.ex_count
            if _count_0 <= rhm_dh.threshold:
                continue

            _count_0 = float(_count_0) / float(rhm_dh.v_excount)
            _count_1 = float(black_dict[key].stats.ex_count) / float(rhm_dh.nv_excount)
            _sum = _count_0 + _count_1
            temp_value = float(_count_0) / float(_sum)

            if temp_value >= rhm_dh.noise_p:
                valid.add(key)

        return valid

    def noise_handling_generalizing(self, rhm_dh, pattern_permutation):

        """
        Handling noise for generalized patterns.
        different from the previous method in term of number of patterns to check.
        e.g. [AD]B --> we have to check patterns [AB, BB, CB, DB]

        :param rhm_dh: data holder, hold the noise, threshold, ...
        :type rhm_dh: RHMDataHolder
        :param pattern_permutation: low lever patterns that form one generalized pattern
        :type pattern_permutation: set
        :return: True if the pattern satisfy the noise percentage
        """

        temp_set_valid = set()
        temp_set_black = set()
        for pat in pattern_permutation:
            if pat in rhm_dh.valid_set:
                temp_set_valid.update(rhm_dh.valid_set[pat].stats.example_set)
            if pat in rhm_dh.black_set:
                temp_set_black.update(rhm_dh.black_set[pat].stats.example_set)

        if len(temp_set_black) == 0:
            return True
        _count_1 = len(temp_set_black) / rhm_dh.nv_excount

        if len(temp_set_valid) <= rhm_dh.threshold:
            return False

        _count_0 = len(temp_set_valid) / rhm_dh.v_excount
        _sum = _count_0 + _count_1

        temp_value = float(_count_0) / float(_sum)

        if temp_value >= rhm_dh.noise_p:
            return True

        return False

    # Wild card
    def wild_card(self, curr_var, rhm_dh, pos_dict):

        """
        Generate the acceptable wild patterns from the initial set of learned patterns
        according to the pattern window length.
        e.g. ABCD --> accepted wild could be: [A.CD, AB.D, A..D]

        :param curr_var: current variable [label:variable]
        :type curr_var: list
        :param rhm_dh: data holder, hold the noise, threshold, ...
        :type rhm_dh: RHMDataHolder
        :param pos_dict: A dictionary of dictionary (Letter/bin --> position --> {patterns})
        :type pos_dict: dictionary
        :return list of acceptable wild patterns
        """

        wild_set = dict()

        for pat in sorted(rhm_dh.valid_set):
            pat_wilds = self.pattern_to_wild(pat, rhm_dh.bins, curr_var, len(rhm_dh.valid_set[pat].p_min))
            for wild_pat in sorted(pat_wilds):
                if wild_pat not in wild_set:
                    new = self.strpat_to_pattern(str(wild_pat))
                    pos_keys = self.pattern_to_pos_keys(new)
                    post_dict_window = pos_dict[len(new)]
                    cons_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
                    permutation = set.union(*cons_pats)
                    for i in range(1, len(pos_keys)):
                        cons_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
                        cons_pats = set.union(*cons_pats)
                        permutation = permutation.intersection(cons_pats)

                    if int(rhm_dh.noise_p) == 1:
                        for _pat in permutation:
                            if _pat in rhm_dh.black_set:
                                not_black = False
                                break
                        else:
                            not_black = True
                    else:
                        not_black = self.noise_handling_generalizing(rhm_dh, permutation)

                    if not_black:
                        wild_set[wild_pat] = True
                    else:
                        wild_set[wild_pat] = False

        return {pat for pat in wild_set if wild_set[pat]}

    def pattern_to_wild(self, pattern, bins, curr_var, pat_length):

        """
        Generate the wild candidates
        :param pattern: initial pattern
        :param bins: list of variables bins
        :param curr_var: current variable [label:variable]
        :param pat_length: pattern length (window-length)
        :return: list of string of the candidates
        """

        wilds = set()
        w = '[A' + Rh.to_chr(bins[curr_var[0]] - 1) + ']'
        if pat_length < 3:
            wilds.add(pattern)
            return wilds

        if pat_length > 4:
            div = int(pat_length / 2)

            if pat_length % 2 == 0:
                _range = range(div - 2, div + 2)
                wilds.add(pattern[0:div - 2])
                suffix = pattern[div + 2:]
            else:
                _range = range(div - 1, div + 2)
                wilds.add(pattern[0:div - 1])
                suffix = pattern[div + 2:]
        elif pat_length == 3:
            _range = range(1, 2)
            wilds.add(pattern[0])
            suffix = pattern[2]
        else:
            _range = range(1, 3)
            wilds.add(pattern[0])
            suffix = pattern[3]

        for ind in _range:
            ch = pattern[ind]
            temp_wilds = []
            for pat in wilds:
                temp_wilds.append(pat + ch)
                temp_wilds.append(pat + '.')
            wilds = temp_wilds

        wilds = [wild + suffix for wild in wilds]
        results = []
        if len(curr_var) > 1:
            sec_loc = int(pat_length / 2)
            w_1 = [wild[:sec_loc] for wild in wilds]
            w_2 = [wild[sec_loc:] for wild in wilds]
            w_1 = [wild.replace('.', w) for wild in w_1]
            w2 = '[A' + Rh.to_chr(bins[curr_var[1]] - 1) + ']'
            w_2 = [wild.replace('.', w2) for wild in w_2]
            for i in range(len(w_1)):
                results.append(w_1[i] + w_2[i])
        else:
            results = [wild.replace('.', w) for wild in wilds]
        return results

    def strpat_to_pattern(self, str_pat):

        """
        Create a GeneralizedPattern from a string representation of a pattern
        :param str_pat: the string representation
        :return: GeneralizedPattern
        """

        p_min = ''
        p_max = ''
        check = False
        ind = 0
        while ind < len(str_pat):
            ch = str_pat[ind]
            if ch == '[':
                check = True
                ind += 1
                continue
            if ch == ']':
                ind += 1
                continue
            if check:
                p_min += ch
                ind += 1
                p_max += str_pat[ind]
                check = False
            else:
                p_min += ch
                p_max += ch
            ind += 1

        return Gp.GeneralizedPattern(p_min, p_max)

    # Other different operations
    def build_position_dictionary(self, patterns):

        pat_pos_dict = dict()

        if patterns is None:
            return pat_pos_dict

        for pat in patterns:
            pat_len = len(pat)
            if pat_len not in pat_pos_dict:
                pat_pos_dict[pat_len] = [dict() for _ in range(pat_len)]

            for i in range(pat_len):
                curr_struct = pat_pos_dict[pat_len][i]
                ch = pat[i]
                if ch not in curr_struct:
                    curr_struct[ch] = set()
                if pat not in curr_struct[ch]:
                    curr_struct[ch].add(pat)
        return pat_pos_dict

    def pattern_to_pos_keys(self, pattern):
        keys = []
        for i in range(len(pattern)):
            pos_set = set()
            min_ch = pattern.p_min[i]
            max_ch = pattern.p_max[i]
            temp_ch = min_ch
            while temp_ch <= max_ch:
                pos_set.add(temp_ch)
                temp_ch = chr(ord(temp_ch) + 1)
            keys.append(pos_set)
        return keys

    def string_permutation(self, string_1, string_2):

        """
        e.g. pat1: ABC, pat2: CBA
        --> generate [ABA, ABB, ABC, BBA, BBB, BBC, CBA, CBB, CBC]
        :param string_1: first pattern
        :param string_2: second pattern
        :return: list of patterns between the two input patterns

        """
        permutation = set()

        if string_1 is not None and string_2 is None:
            permutation.add(string_1)
            return permutation

        if string_1 is None:
            return permutation

        ch_1 = string_1[0]
        ch_2 = string_2[0]
        for j in range(ord(ch_1), ord(ch_2) + 1):
            permutation.add(chr(j))

        for i in range(1, len(string_1)):
            ch_1 = string_1[i]
            ch_2 = string_2[i]
            perm_temp = set()
            for j in range(ord(ch_1), ord(ch_2) + 1):
                for pat in permutation:
                    perm_temp.add(pat + chr(j))
            permutation = perm_temp

        return permutation

    def all_possibility(self, min_string, max_string):

        """
        Calculate the number of patterns that fall between two patterns
        :param min_string: first pattern
        :param max_string: second pattern
        :return:
        """

        res = 1
        for a, b in zip(min_string, max_string):
            res *= ord(b) - ord(a) + 1
        return res

    def aggregate_patterns(self, pattern_set, learned_pattern):

        """
        To make things simpler, in all the steps, we only learned the string representation
        of the good pattern. This method calculate the pattern relative statistic, coverage, locations, ...
        :param pattern_set: initial set of learned patterns (with statistics)
        :param learned_pattern: learned patterns (no statistics)
        :return: the learned patterns with their related statistics
        """

        result = dict()
        for pat in pattern_set:
            g_pat = pattern_set[pat]
            for l_pat in learned_pattern:
                if l_pat not in result:
                    gl_pat = self.strpat_to_pattern(l_pat)
                    result[l_pat] = gl_pat
                else:
                    gl_pat = result[l_pat]
                if gl_pat == g_pat:
                    gl_pat.stats.merge(copy.deepcopy(g_pat.stats))

        return result

    def aggregate_patterns_black(self, rhm_dh, learned_patterns, pos_dict):

        """
        To make things simpler, in all the steps, we only learned the string representation
        of the good pattern. This method calculate the pattern relative statistic, coverage, locations, ...
        :param learned_patterns: learned patterns (no statistics)
        :param rhm_dh: data holder, hold the noise, threshold, ...
        :type rhm_dh: RHMDataHolder
        :param pos_dict: A dictionary of dictionary (Letter/bin --> position --> {patterns})
        :type pos_dict: dictionary
        :return: the learned patterns with their related statistics
        """

        result = dict()
        for l_pat in learned_patterns:

            new = self.strpat_to_pattern(l_pat)
            pos_keys = self.pattern_to_pos_keys(new)
            post_dict_window = pos_dict[len(new)]
            cons_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
            permutation = set.union(*cons_pats)
            for i in range(1, len(pos_keys)):
                cons_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
                cons_pats = set.union(*cons_pats)
                permutation = permutation.intersection(cons_pats)

            for pat in permutation:
                if pat in rhm_dh.valid_set:
                    new.stats.merge(copy.deepcopy(rhm_dh.valid_set[pat].stats))
                if pat in rhm_dh.black_set:
                    new.stats.black_merge(copy.deepcopy(rhm_dh.black_set[pat].stats))
            result[str(new)] = new

        return result

    def remove_rotation(self, p_set):

        """
        Remove rotated pattern from a set of patterns.
        e.g. of rotation: ABAB and BABA, AA and AAB.
        We remove one of the two patterns just when they cover the same examples,
        of one pattern coverage falls completely in the other one.
        Finally, we keep the shorter pattern (length) or the pattern with more coverage.
        :param p_set: Set of patterns
        :return: Filtered set of patterns
        """

        set_copy = copy.deepcopy(set(p_set.keys()))
        for pat1, pat2 in itertools.combinations(sorted(set_copy), 2):
            if pat1 in p_set and pat2 in p_set:
                set_1 = p_set[pat1].stats.example_set
                set_2 = p_set[pat2].stats.example_set
                if len(p_set[pat1]) == len(p_set[pat2]) and pat1 in pat2*2:
                    if set_1.issubset(set_2):
                        del p_set[pat1]
                    elif set_2.issubset(set_1):
                        del p_set[pat2]
                elif len(p_set[pat1]) > len(p_set[pat2]) and pat2 in pat1:
                    if set_1.issubset(set_2):
                        del p_set[pat1]
                else:
                    if len(p_set[pat1]) < len(p_set[pat2]) and set_2.issubset(set_1):
                        del p_set[pat2]

        return p_set

    def remove_duplicates(self, learned_set):

        """
        Remove the duplicate patterns from the set of learned patterns.
        duplicate doesn't only mean the exact same pattern but also
        it remove the patterns that are included in some other patterns.
        e.g. [BD] and [AD] --> we remove [BD]
        :param learned_set: set of patterns to be filtered
        :return: Filtered set
        """

        set_copy = copy.deepcopy(learned_set)
        for pat1, pat2 in itertools.combinations(set_copy, 2):
            if pat1 not in learned_set or pat2 not in learned_set:
                continue
            temp_pat_1 = self.strpat_to_pattern(pat1)
            temp_pat_2 = self.strpat_to_pattern(pat2)
            if len(temp_pat_1.p_min) != len(temp_pat_2.p_min):
                continue

            dist = temp_pat_1.dist(temp_pat_2)
            if dist == 0:
                comp = temp_pat_1.compare_coverage(temp_pat_2)
                if comp == 0:
                    continue
                elif comp == 1:
                    learned_set.remove(pat2)
                else:
                    learned_set.remove(pat1)

        return learned_set

    # Pattern generalization step (optional)
    def pattern_generalizer(self, valid_patterns, rhm_dh, pos_dict):

        """
        Computes the agglomerative clustering of the pattern_set using generalisedPattern.dist as
        the distance and merge elements using the generalize function

        :param valid_patterns: the set of GeneralizedPattern
        :type valid_patterns: an iterable (list or array)
        :param black_list: set of pattern that should not be generated
        :type black_list: collection that support "pattern in col" operator
        :param percent: acceptable noise percentage
        :type float number
        :return: a linkage matrix
        :rtype: list of tuples of the form (i, j, pos_link, pos_pattern_set): \
         patterns i, and j of pattern_set i and j are merged into a new pattern. This new pattern \
         is available at pattern_set[pos_pattern_set]. \
         Merges that are operate on this new pattern are available at linkage[pos_link].
        """

        mat_dissim = collections.OrderedDict()
        min_dist = sys.float_info.max
        best = (None, None)
        for key_i, key_j in itertools.combinations(sorted(valid_patterns.keys()), 2):
            if len(key_i) != len(key_j):
                continue
            dist = valid_patterns[key_i].dist(valid_patterns[key_j])
            key = '$' + key_i + '$:$' + key_j + '$'
            if dist in mat_dissim:
                mat_dissim[dist].append(key)
            else:
                mat_dissim[dist] = []
                mat_dissim[dist].append(key)
            if dist < min_dist:
                min_dist = dist
                best = (key_i, key_j)
        to_merge = len(valid_patterns)

        if best == (None, None):
            return valid_patterns

        while to_merge > 1:
            (b_i, b_j) = best
            #creating the new element
            new = self.generalize(valid_patterns[b_i], valid_patterns[b_j])
            #print(new, valid_patterns[b_i], valid_patterns[b_j])
            pos_keys = self.pattern_to_pos_keys(new)
            post_dict_window = pos_dict[len(new)]
            consid_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
            permutation = set.union(*consid_pats)
            for i in range(1, len(pos_keys)):
                consid_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
                consid_pats = set.union(*consid_pats)
                permutation = permutation.intersection(consid_pats)

            if rhm_dh.noise_p < 0:
                for pat in permutation:
                    if pat in rhm_dh.black_set:
                        not_black = False
                        break
                else:
                    not_black = True

            else:
                not_black = self.noise_handling_generalizing(rhm_dh, permutation)

            if not not_black:
                min_dist = min(mat_dissim.keys())
                key = '$' + b_i + '$:$' + b_j + '$'
                mat_dissim[min_dist].remove(key)
            else:
                del valid_patterns[b_i]
                del valid_patterns[b_j]
                temp_b_i_1 = '$' + b_i + '$:'
                temp_b_i_2 = ':$' + b_i + '$'
                temp_b_j_1 = '$' + b_j + '$:'
                temp_b_j_2 = ':$' + b_j + '$'
                for k, v in mat_dissim.items():
                    mat_dissim[k] = [x for x in v if temp_b_i_1 not in x and temp_b_i_2 not in x
                                     and temp_b_j_1 not in x and temp_b_j_2 not in x]

                for key in [x for x in sorted(valid_patterns.keys()) if len(x) == len(new.p_min)]:
                    dist = valid_patterns[key].dist(new)
                    if key < str(new):
                        _temp_key = '$' + key + '$:$' + str(new) + '$'
                    else:
                        _temp_key = '$' + str(new)+ '$:$' + key + '$'
                    if dist in mat_dissim:
                        mat_dissim[dist].append(_temp_key)
                    else:
                        mat_dissim[dist] = []
                        mat_dissim[dist].append(_temp_key)

                valid_patterns[str(new)] = new
                to_merge -= 1

            mat_dissim = {k: v for k, v in mat_dissim.items() if len(v) > 0}

            # recompute the minimum dist
            if len(mat_dissim) == 0:
                break
            min_dist = min(mat_dissim.keys())
            best_key = mat_dissim[min_dist][0]
            best_key = best_key.split(':')
            best = (best_key[0].replace('$', ''), best_key[1].replace('$', ''))

        return valid_patterns

    # For testing
    def merge_timeseries(self, data_to_symbols, variables, windows_list):

        """
        Used for classification, combine two string representation of two timeseries into one
        big string according to a specific window size separated by ':'
        e.g. [AAABB, BBBCC] with w = 3 --> 'AAABBB:AABBBC:ABBBCC'
        :param data_to_symbols: string representation of the timeseries
        :param variables: pair of variables to take into account
        :param windows_list: list of windows (each variable has its own window size)
        :return: string
        """

        nb_windows = len(data_to_symbols[variables[0]])
        var = 0
        _series = ''
        for i in range(nb_windows - windows_list[var] + 1):
            for j in range(len(windows_list)):
                _series += ''.join([x for x in data_to_symbols[variables[j]][i:i + windows_list[j]]])
            _series += ':'
        return _series

    def rhm_data_prepare(self, ls_table_name=None, var_pair=False, id_label='flight_id', id_class='target'):

        # Data reader
        _ls_builder = Tsls.LearningSetBuilder()
        _Data = _ls_builder.build_from_ikats(ls_table_name, id_label=id_label, id_class=id_class)
        var_names = _Data.variables

        self.train_data_len = len(_Data)
        names = dict()
        for i, j in enumerate(var_names[1:]):
            names[str(i)] = j

        for i in range(len(var_names[1:])):
            self.all_variables.append(str(i))
        if len(names) > 1 and var_pair:
            for i, j in itertools.combinations(range(len(names)), 2):
                self.all_variables.append(str(i) + '-' + str(j))

        if len(names) > 1 and var_pair:
            for i, j in itertools.combinations(range(len(names)), 2):
                names[str(i) + '-' + str(j)] = names[str(i)] + '-' + names[str(j)]
        self.names = names

        return _Data

    def rhm_train_ikats(self, learningset=None,
                        bins=None,
                        labels=None,
                        purity=-1.0,
                        cov_threshold=0,
                        windows=None,
                        generalizer=False,
                        var_pair=True,
                        conf_threshold=1.0):

        if labels is not None:
            all_labels = set(learningset.get_labels())
            if not labels.issubset(all_labels):
                raise ValueError("Input labels are not dataset labels")
            learningset.cut_labels(labels)
        else:
            labels = set(learningset.get_labels())

        logger.debug('Labels are correct...')

        # Copy the data
        _data_ts = deepcopy(learningset)
        _data_ts.subset_variable([0])
        nb_vars = learningset.nb_variables()
        learningset.subset_variable(list(range(1, nb_vars)))

        for i in range(nb_vars - 2):
            bins.append(bins[0])

        # Discretizing the data
        _discretizer = Ed.Discretizer()
        _discretizer.compute_breakpoints(learningset, bins)
        _discretizer.discretize(learningset)
        logger.debug('Discretizing done successfully...')

        _stats = dict()

        # Compute patterns for each window (one variable)
        _temp_stats = self.compute_stats(learningset, windows)

        # Save all patterns in one object
        for _stat in _temp_stats:
            if _stat in _stats:
                _stats[_stat].update(_temp_stats[_stat])
            else:
                _stats[_stat] = _temp_stats[_stat]

        # Compute patterns for each windows (pair of variables)
        if var_pair:
            windows_list = []
            for w in windows:
                if w % 2 == 0:
                    windows_list = [int(w / 2), int(w / 2)]
                _temp_stats_mv = self.compute_stats_multivar(learningset, windows_list)

                # Save the patterns in the _stats object
                for _stat in _temp_stats_mv:
                    if _stat in _stats:
                        _stats[_stat].update(_temp_stats_mv[_stat])
                    else:
                        _stats[_stat] = _temp_stats_mv[_stat]

        logger.debug('Finished calculating the bag of patterns...')

        all_valid = dict()
        all_black = dict()

        labels_examples = dict()
        # Count the coverage of each label
        for label in labels:
            self.labels_coverage[label] = len([x for x in learningset.get_labels() if x == label])
            labels_examples[label] = {i for i in range(len(learningset)) if learningset.get_label(i) == label}

        visu = dict()
        if len(self.names) > 1:
            visu["break_points"] = [list(x) for x in _discretizer.breakpoints]
        else:
            visu["break_points"] = [list(_discretizer.breakpoints[0])]

        visu['disc_break_points'] = []
        for b in bins:
            lst = []
            for i in range(0, b):
                lst.append(Rh.to_chr(i))
            visu['disc_break_points'].append(lst)

        visu_pattern = dict()

        # for each label, learn their relative patterns
        for i in sorted(labels):
            _labels = [i]
            _labels.extend([j for j in labels if i != j])
            c0 = _labels[0]
            c1 = _labels[1:]

            v_excount = self.labels_coverage[c0]
            nv_excount = len(learningset) - v_excount
            threshold = round(v_excount * cov_threshold)
            nthreshold = round(nv_excount * cov_threshold)
            conf_threshold = round(v_excount * conf_threshold)

            # RHMDataHolder initialization, this was done to reduce the number of parameters
            rhm_dh = Rdh.RHMDataHolder(c0, c1, labels_examples, v_excount, nv_excount, bins, None, None,
                                       windows, None, purity, threshold, conf_threshold)

            # for each variable, learn their relative patterns
            for var in self.all_variables:

                valid = dict()
                black_dict = dict()

                logger.debug('Finding patterns for class %s variable %s...' % (c0, var))
                logger.debug('Removing false positive and false negative...')

                # Compute the non-valid patterns
                for c in c1:
                    black_str = str(c) + ':' + var
                    temp_threshold = round(self.labels_coverage[c] * cov_threshold)
                    if black_str in _stats:
                        for p, value in _stats[black_str].items():
                            if value.ex_count > temp_threshold:
                                if p in black_dict:
                                    black_dict[p].stats.merge(value)
                                else:
                                    black_dict[p] = Gp.GeneralizedPattern(p, stats=deepcopy(value))

                black_dict = {p: black_dict[p] for p in black_dict if black_dict[p].stats.ex_count > nthreshold}
                black_list_keys = black_dict.keys()

                # for printing
                curr_vars = [int(x) for x in var.split('-')]
                _temp_lab_var = c0 + ':' + self.names[str(curr_vars[0])]
                if len(curr_vars) > 1:
                    _temp_lab_var = _temp_lab_var + '-' + self.names[str(curr_vars[1])]

                # Compute the valid-patterns
                valid_str = str(c0) + ':' + var
                if valid_str in _stats:
                    pattern_array = [key for key in _stats[valid_str].keys() if key not in black_list_keys
                                     and _stats[valid_str][key].ex_count > rhm_dh.threshold]
                    valid.update({p: Gp.GeneralizedPattern(p, stats=_stats[valid_str][p]) for p in pattern_array})

                logger.debug('Removing noisy patterns...')
                # Handling noise
                if 0 <= rhm_dh.noise_p <= 1:
                    noisy_valid_pattern = self.noise_handling_pregeneralize(_stats[valid_str], black_dict, rhm_dh)
                    for key in sorted(noisy_valid_pattern):
                        valid[key] = Gp.GeneralizedPattern(key, stats=_stats[valid_str][key])

                rhm_dh.valid_set = valid
                rhm_dh.black_set = black_dict
                # End of handling noise

                if len(valid) > 500:
                    logger.warning('The number of low-level patterns are big\n, process might take some time')

                temp_set = set(valid.keys())
                temp_set.update(set(black_dict.keys()))
                pos_dict = self.build_position_dictionary(temp_set)

                logger.debug('Generating wild patterns...')
                # Generating wild-patterns
                wilds = self.wild_card(curr_vars, rhm_dh, pos_dict)
                wilds = self.remove_duplicates(wilds)
                # Expanding patterns
                # expand_valid = self.expand_patterns(wilds, curr_vars, rhm_dh, pos_dict)

                logger.debug('Expanding the patterns (best of one step)...')
                _dbins = var.split('-')
                dbins = [Rh.to_chr(bins[int(i)] - 1) for i in _dbins]
                expand_valid = set()
                for pat in wilds:
                    gpat = self.strpat_to_pattern(pat)
                    length = len(gpat.p_min)
                    if len(dbins) > 1:
                        windows_list = [int(length/2), int(length/2)]
                    else:
                        windows_list = [length]
                    _temp_pattern = self.expand_ranges(gpat, dbins, windows_list, rhm_dh, pos_dict)

                    if str(_temp_pattern) not in expand_valid:
                        expand_valid.add(str(_temp_pattern))

                logger.debug('Removing dupplicated patterns...')
                # Combine the wilds patterns and the expanded patterns
                wilds = expand_valid.union(wilds)
                # Remove the duplicates
                wilds = self.remove_duplicates(wilds)
                # Aggregate the pattern stats objects
                valid = self.aggregate_patterns_black(rhm_dh, wilds, pos_dict)
                logger.debug('Removing rotations...')
                valid = self.remove_rotation(valid)

                # Generalize the patterns (few patterns)
                if generalizer:
                    logger.debug('Generalizing patterns (hierarchical clustering)...')
                    valid = self.pattern_generalizer(valid, rhm_dh, pos_dict)
                rhm_dh.valid_set = valid

                if len(valid) > 0:
                    logger.debug('Tiling patterns...')
                    indices = self.greedy_tiling(rhm_dh)
                    # indices = self.tiling(rhm_dh, self.train_data_len)
                    valid = {k: valid[k] for k in indices}

                    ex_set_var = set()
                    logger.info(_temp_lab_var + '\n')
                    for pat in sorted(valid):
                        ex_set_var.update(valid[pat].stats.example_set)
                        string_pat = Rh.pattern_to_re2(pat, Rh.to_chr(bins[int(curr_vars[0])] - 1))
                        logger.info(string_pat + ' ' + str(sorted(valid[pat].stats.example_set)) + '\n')
                        logger.info(string_pat + ' ' + str(sorted(valid[pat].stats.example_bset)) + '\n')

                        vis_pat = dict()
                        print_pat = Rh.pattern_to_re2(pat, Rh.to_chr(bins[int(curr_vars[0])] - 1))
                        vis_pat["key"] = print_pat
                        if '-' in var:
                            _pat_length = len(valid[pat].p_min) / 2
                        else:
                            _pat_length = len(valid[pat].p_min)
                        vis_pat["length"] = _pat_length
                        if '-' in var:
                            print_pat = Rh.pattern_to_re(print_pat, len(valid[pat].p_min))
                        vis_pat["regex"] = print_pat
                        vis_pat["class"] = c0
                        vis_pat["variable"] = _temp_lab_var.split(':')[1]
                        locations = dict()
                        for ex in valid[pat].stats.example_set:
                            sorted_pos = sorted(list(valid[pat].stats.example_pos_set(ex)))
                            time_stamp_ts = [_ts for _ts in _data_ts if _ts.id == ex][0]
                            set_pos = self.get_index_timestamp_pair(sorted_pos, time_stamp_ts.data(), _pat_length)
                            locations[ex] = set_pos
                        for ex in valid[pat].stats.example_bset:
                            sorted_pos = sorted(list(valid[pat].stats.example_pos_bset(ex)))
                            time_stamp_ts = [_ts for _ts in _data_ts if _ts.id == ex][0]
                            b_set_pos = self.get_index_timestamp_pair(sorted_pos, time_stamp_ts.data(), _pat_length)
                            locations[ex] = b_set_pos
                        vis_pat["locations"] = locations
                        visu_pattern[print_pat] = vis_pat

                    logger.debug('all_valid:  %s/%s\n' % (len(ex_set_var), v_excount))
                    logger.debug('\n')

                logger.debug('Done learning patterns for class %s...' % c0)
                all_valid[valid_str] = valid
                all_black[valid_str] = black_dict

        visu["variables"] = [self.names[i] for i in self.all_variables]
        visu["patterns"] = visu_pattern
        self.valid_patterns = all_valid
        self.black_patterns = all_black

        return visu

    def get_index_timestamp_pair(self, positions, timestamps, pat_length):

        results = []
        for pos in positions[:20]:
            duration = timestamps[pos + pat_length - 1][0] - timestamps[pos][0]
            results.append({"timestamp": timestamps[pos][0], "pos": pos, "duration": duration})
        return results
