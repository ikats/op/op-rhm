import numpy as np
import re


def data_to_vis(_data, ucrd=False, names=None):
    res = dict()
    for i in range(len(_data)):
        data = []
        meta_min = []
        meta_max = []
        ts_data = dict()
        ts_data['class'] = _data.get_label(i)
        ts_data['meta_data'] = dict()
        if _data[i].nb_variables() == 1:
            data.append([x[0] for x in np.array([_data[i].data()], dtype=np.float64).T[0]])
            meta_min.append(min(data[0]))
            meta_max.append(max(data[0]))
        else:
            if not ucrd:
                for j in range(_data[i].nb_variables()):
                    data.append([x for x in np.array(_data[i].data().T)[j]])
                    meta_min.append(min(data[j]))
                    meta_max.append(max(data[j]))
            else:
                for j in range(_data[i].nb_variables()):
                    data.append([x for x in np.array(_data[i].data().T)[0][j]])
                    meta_min.append(min(data[j]))
                    meta_max.append(max(data[j]))

        ts_data['meta_data']['min_vals'] = meta_min
        ts_data['meta_data']['max_vals'] = meta_max
        ts_data['data'] = data
        res['TS' + str(i)] = ts_data
    return res


def data_disc_to_vis(_data, names=None):
    res = dict()
    for i in range(len(_data)):
        data = []
        meta_min = []
        meta_max = []
        ts_data = dict()
        ts_data['class'] = _data.get_label(i)
        ts_data['meta_data'] = dict()
        for j in range(_data[i].nb_variables()):
            data.append(_data[i].data_symbols[j])
            meta_min.append(min(list(data[j])))
            meta_max.append(max(list(data[j])))

        ts_data['meta_data']['min_vals'] = meta_min
        ts_data['meta_data']['max_vals'] = meta_max
        ts_data['data'] = data
        res['TS' + str(i)] = ts_data
    return res


def pattern_to_re(pattern, length):
    _bool = False
    index = 0
    char_pos = -1
    length_div = length / 2
    for c in pattern:
        char_pos += 1
        if index == length_div and not _bool:
            break
        if c == ']':
            _bool = False
            continue
        if _bool:
            continue
        if c == '[':
            index += 1
            _bool = True
            continue
        index += 1

    pattern = pattern[0:char_pos] + '$' + pattern[char_pos:]
    return pattern


def pattern_to_re2(pattern, upper_bin):
    point_reg = '[A' + upper_bin + ']'
    return pattern.replace(point_reg, '.')


def pattern_to_re3(pattern):
    _bool = False
    _res = ''
    for c in pattern:
        if _bool:
            _res += c + '-'
            _bool = False
            continue
        if c == '[':
            _bool = True
        _res += c
    return _res


def pattern_matcher(pattern, length, string):
    index = -1
    positions = []
    while len(string) >= length:
        res = re.search(pattern, string)
        if res:
            index += res.start() + 1
            positions.append(index)
            string = string[res.start() + 1:]
        else:
            break
    return positions


def to_int(char):
    """
    Converts char to its integer value.
    For char in [A-Z], char is matches to i such 'A'+i = char.
    else it is matches with lower case values
    :param char: character
    :type char: string of length 1
    :return: the corresponding int value
    :rtype: an int
    """
    return ord(char) - ord('A') if char <= 'Z' else 26 + (ord(char) - ord('a'))


def to_chr(x):
    """
    Converts an int to a char. If the int is larger than 25
    we switch to the lower version of the char.
    :param x: a discrete value (typically small)
    :type x: a positive integer
    :return: a character representing the integer
    :rtype: string (as return by chr)
    """
    # 71 = 97-26, 91 ascii number of "a", 26 letters
    return chr(x+71) if x > 25 else chr(x+65)