
class PatternStatistics(object):
    """
    Class that represents pattern statistics.
    For each class,  we collect the following patterns statistics:
    - count: the number of times it appears in the whole serie set
    - excount the number of examples in which it appears
    - avg_pos position in which it appears (in the whole database)
    - avg_gap average gap between two appearances
    """

    def __init__(self):
        """
        initializes the object to an empty one (all counts to 0, pred to  None).
        """
        self._count = 0                 # total count of the pattern occurrences (valid)
        self._b_count = 0               # total count of the pattern occurrences (non-valid)
        self._ex_count = 0              # number of example (TS) the patterns occures in
        self._b_ex_count = 0            # number of example (TS) the patterns occures in (non-valid)

        # Some other statistics
        self._sum_pos = 0
        self._count_gap = 0
        self._sum_gap = 0
        self._pred = None

        self._example_set = dict()      # (TS--> Positions) (valid)
        self._example_bset = dict()     # (TS--> Positions) (non-valid)

    @property
    def count(self):
        return self._count

    @count.setter
    def count(self, value):
        self._count = value

    @property
    def b_count(self):
        return self._b_count

    @b_count.setter
    def b_count(self, value):
        self._b_count = value

    @property
    def ex_count(self):
        return self._ex_count

    @ex_count.setter
    def ex_count(self, value):
        self._ex_count = value

    @property
    def b_ex_count(self):
        return self._b_ex_count

    @b_ex_count.setter
    def b_ex_count(self, value):
        self._b_ex_count = value

    @property
    def sum_pos(self):
        return self._sum_pos

    @sum_pos.setter
    def sum_pos(self, value):
        self._sum_pos = value

    @property
    def count_gap(self):
        return self._count_gap

    @count_gap.setter
    def count_gap(self, value):
        self._count_gap = value

    @property
    def avg_pos(self):
        return self._sum_pos/self._count

    @property
    def sum_gap(self):
        return self._sum_gap

    @sum_gap.setter
    def sum_gap(self, value):
        self._sum_gap = value

    @property
    def pred(self):
        return self._pred

    @pred.setter
    def pred(self, value):
        self._pred = value

    def labeled_avg_pos(self, ex_set):
        """
        Average position in a specific group if examples
        :param ex_set: the example set
        :return:
        """
        _sum = 0
        for ex in ex_set:
            _sum += sum(self._example_set[ex])
        return _sum / len(ex_set)

    @property
    def example_set(self):
        """
        getter for the example set
        :return: the example set (the set of example id where patter appears)
        :rtype: set
        """
        return set(self._example_set.keys())

    def example_pos_set(self, ex):
        """
        Return the set of positions for a specific example
        :rtype: set
        """
        return self._example_set[ex]

    @property
    def example_bset(self):
        """
        getter for the black example set
        :return: the example set (the set of example id where patter appears)
        :rtype: set
        """
        return set(self._example_bset.keys())

    def example_pos_bset(self, ex):
        """
        Return the set of positions for a specific non-valid example
        :rtype: set
        """
        return self._example_bset[ex]

    @property
    def set(self):
        """
        return a set of all examples (valid and non-valid)
        :return:
        """
        z = self._example_set.copy()
        z.update(self._example_bset)
        return z

    def __str__(self):
        """print the statistics"""
        _stats = self.get_statistics()
        return "count={:<5} ex_count={:<5} avg_pos={:.2f} avg_gap={} set={}".format(_stats[0],\
                                                                      _stats[1],\
                                                                      _stats[2],\
                                                                      _stats[3],\
                                                                       str(self._example_set.keys()))

    def __repr__(self):
        return self.__str__()

    def appears(self, example, at):

        """
        Update the statistics with the appearance of the pattern given it appears at pos *at* in
        example *example*.
        If example_set is None, it means that the set is not discriminative -> do not update it.

        :param example: the example id in which the pattern appears.
        :param at: the position in which it appears in the given example.

        """

        self._count += 1
        self._sum_pos += at

        if self._pred is None:
            self._ex_count += 1
            self._pred = at
        else:
            self._count_gap += 1
            self._sum_gap += (at - self._pred)

        self._pred = at
        self._example_set[example].add(at)

    def new_example(self, example):

        """
        reset the statistics related to one given example (like *e.g.* the
        previous position in an example )
        """

        if self._example_set is not None:
            self._example_set[example] = set()
        self._pred = None

    def get_statistics(self):

        """
        returns the pattern statistics

        :return: the pattern statistics (count, ex_count, avg_pos, avg_gap)
        :return type: tuple
        """

        _avg_pos = None if self._count == 0 else self._sum_pos/self._count
        _avg_gap = None if self._count_gap == 0 else self._sum_gap/self._count_gap
        return (self._count, self._ex_count, _avg_pos, _avg_gap)

    def merge(self, other):

        """
        Merge the statistics from other into self (counts are updated).

        Assumes that they were computed on different examples sets
        with same label and same variable. Reset the _pred member.

        :param other: that statistics to merge with.
        :type other: PatternStatistics
        """

        self._count += other.count
        self._sum_pos += other.sum_pos
        self._count_gap += other.count_gap
        self._sum_gap += other.sum_gap
        for example in other.example_set:
            if example not in self.example_set:
                self._example_set[example] = other.example_pos_set(example)
            else:
                self.example_pos_set(example).update(other.example_pos_set(example))
        self._ex_count = len(self.example_set)
        self._pred = None

    def black_merge(self, other):

        """
        Merge the statistics from other into self (counts are updated) (non-valid statistics).

        Assumes that they were computed on different examples sets
        with same label and same variable. Reset the _pred member.

        :param other: that statistics to merge with.
        :type other: PatternStatistics
        """

        count = 0
        for example in other.example_set:
            if example not in self.example_bset:
                self._example_bset[example] = other.example_pos_set(example)
            else:
                self.example_pos_bset(example).update(other.example_pos_set(example))
            count += len(other.example_pos_set(example))
        self._b_ex_count = len(self.example_bset)
        self._b_count = count
