class RHMDataHolder(object):
    """
    This class stores the information needed to run RHM.
    """
    def __init__(self, v_label, nv_labels, labels_examples, v_excount, nv_excount, bins,
                valid_set, black_set, windows_list, pair_windows, noise_p, threshold, conf_threshold):

        """
        :param v_label:             valid-label
        :param nv_labels:           non-valid labels
        :param labels_examples:     dictionary (label --> set of examples)
        :param v_excount:           number of valid examples (this is changed through the process)
        :param nv_excount:          number of non-valid examples (this is changed through the process)
        :param bins:                bins of the current variable
        :param valid_set:           valid patterns (low level)
        :param black_set:           non-valid patterns (low level)
        :param windows_list:        sliding window list
        :param pair_windows:        sliding window list (pair of windows)
        :param noise_p:             noise percentage
        :param threshold:           minimum accepted percentage of coverage
        :param conf_threshold:      minimum accepted percentage of coverage (after tiling)
        """

        self._v_label = v_label
        self._nv_labels = nv_labels
        self._v_excount = v_excount
        self._nv_excount = nv_excount
        self._bins = bins
        self._valid_set = valid_set
        self._black_set = black_set
        self._windows_list = windows_list
        self._pair_windows = pair_windows
        self._noise_p = noise_p
        self._threshold = threshold
        self._labels_examples = labels_examples
        self._conf_threshold = conf_threshold

    # Setters and Getters

    @property
    def v_label(self):
        return self._v_label

    @v_label.setter
    def v_label(self, value):
        self._v_label = value

    @property
    def nv_labels(self):
        return self._nv_labels

    @nv_labels.setter
    def nv_labels(self, value):
        self._nv_labels = value

    @property
    def labels_examples(self):
        return self._labels_examples

    @property
    def v_excount(self):
        return self._v_excount

    @v_excount.setter
    def v_excount(self, value):
        self._v_excount = value

    @property
    def nv_excount(self):
        return self._nv_excount

    @nv_excount.setter
    def nv_excount(self, value):
        self._nv_excount = value

    @property
    def bins(self):
        return self._bins

    @bins.setter
    def bins(self, value):
        self._bins = value

    @property
    def valid_set(self):
        return self._valid_set

    @valid_set.setter
    def valid_set(self, value):
        self._valid_set = value

    @property
    def black_set(self):
        return self._black_set

    @black_set.setter
    def black_set(self, value):
        self._black_set = value

    @property
    def windows_list(self):
        return self._windows_list

    @windows_list.setter
    def windows_list(self, value):
        self._windows_list = value

    @property
    def pair_windows(self):
        return self._pair_windows

    @pair_windows.setter
    def pair_windows(self, value):
        self._pair_windows = value

    @property
    def noise_p(self):
        return self._noise_p

    @noise_p.setter
    def noise_p(self, value):
        self._noise_p = value

    @property
    def threshold(self):
        return self._threshold

    @threshold.setter
    def threshold(self, value):
        self._threshold = value

    @property
    def avg_length(self):
        return self._avg_length

    @avg_length.setter
    def avg_length(self, value):
        self._avg_length = value

    @property
    def conf_threshold(self):
        return self._conf_threshold

    @conf_threshold.setter
    def conf_threshold(self, value):
        self._conf_threshold = value