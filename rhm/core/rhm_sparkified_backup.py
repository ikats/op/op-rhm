import numpy as np
import copy
import itertools
import gc
import json
import rhm.core.GeneralizedPattern_spark as gp
import rhm.core.Discretizer_spark as ed
import rhm.learningset.LearningSetBuilder_spark as tsls
from copy import deepcopy
from rhm.core import RHMDataHolder as rdh
from rhm.core import RHMDataHandler as rh
from pyspark import SparkContext, SparkConf
from ikatsbackend.core.io.spark import ScManager


"""
This file contains the RHM implementation steps using spark.
For non-spark implementation, check algo/main.py
"""


class RHM(object):

    def __init__(self, config):
        self._all_variables = []
        self._names = []
        self._train_data_len = 0
        self._label_cov_dict = dict()
        self._config = config
        self._valid_patterns = dict()
        self._black_patterns = dict()
        self._visu_raw = dict()
        self._visu_disc = dict()
        self._visu_pattern = dict()
        self._visu_raw_metadata = dict()
        self._visu_disc_metadata = dict()

    @property
    def config(self):
        return self._config

    @property
    def labels_coverage(self):
        return self._label_cov_dict

    @property
    def all_variables(self):
        return self._all_variables

    @property
    def names(self):
        return self._names

    @names.setter
    def names(self, value):
        self._names = value

    @property
    def train_data_len(self):
        return self._train_data_len

    @train_data_len.setter
    def train_data_len(self, value):
        self._train_data_len = value

    @property
    def valid_patterns(self):
        return self._valid_patterns

    @valid_patterns.setter
    def valid_patterns(self, value):
        self._valid_patterns = value

    @property
    def black_patterns(self):
        return self._black_patterns

    @black_patterns.setter
    def black_patterns(self, value):
        self._black_patterns = value

    @property
    def visu_raw(self):
        return self._visu_raw

    @visu_raw.setter
    def visu_raw(self, value):
        self._visu_raw = value

    @property
    def visu_disc(self):
        return self._visu_disc

    @visu_disc.setter
    def visu_disc(self, value):
        self._visu_disc = value

    @property
    def visu_pattern(self):
        return self._visu_pattern

    @visu_pattern.setter
    def visu_pattern(self, value):
        self._visu_pattern = value

    @property
    def visu_raw_metadata(self):
        return self._visu_raw_metadata

    @visu_raw_metadata.setter
    def visu_raw_metadata(self, value):
        self._visu_raw_metadata = value

    @property
    def visu_disc_metadata(self):
        return self._visu_disc_metadata

    @visu_disc_metadata.setter
    def visu_disc_metadata(self, value):
        self._visu_disc_metadata = value

    # Patterns manipulation
    def merge(self, pat_1, pat_2):
        """
        Merge the statistics from other into self (counts are updated).
        Assumes that they were computed on different examples sets
        with same label and same variable. Reset the _pred member.

        :param pat_1: First pattern
        :type pat_1: GeneralizedPattern
        :param pat_2: Second pattern
        :type pat_2: GeneralizedPattern
        """
        stats_1 = pat_1.stats
        stats_2 = pat_2.stats

        stats_1.count += stats_2.count
        stats_1.sum_pos += stats_2.sum_pos
        stats_1.count_gap += stats_2.count_gap
        stats_1.sum_gap += stats_2.sum_gap
        for example in stats_2.example_set:
            if example not in stats_1.example_set:
                stats_1.new_example(example)
            stats_1.example_pos_set(example).update(stats_2.example_pos_set(example))
        stats_1.ex_count = len(stats_1.example_set)
        stats_1.pred = None

        return pat_1

    # Bag of patterns
    def filter_on_coverage(self, pat, label_cov, cov_threshold):
        """

        :param pat: GeneralizedPattern, pattern to check if its coverage satisfy the minimum percentage
        :param label_cov: number of example related the pattern label
        :param cov_threshold: the coverage percentage
        :return: boolean, True if the coverage condition is satisfied
        """
        v_excount = label_cov[pat.label]
        threshold = round(v_excount * cov_threshold)
        return pat.stats.ex_count > threshold

    def pattern_generator(self, arr, variable_id, window_list):
        """
        returns a list of list, where each contains the patterns related
        to a different window size (window_list)
        :param arr: a string representation of the timeseries
        :param variable_id
        :param window_list: a list of window sizes
        :return: a list of list (string) that will enumerate all the patterns.
        """
        results = []
        for window in window_list:
            result = []
            for i in range(len(arr[variable_id]) - window + 1):
                result.append("".join([x for x in arr[variable_id][i:i+window]]))
            results.append(result)
        return results

    def compute_stats(self, ts, window_list):
        """
        Generate the bag of patterns statistics

        :param ts: a timeseries
        :type ts: TimeSeries
        :param window_list: a list of window sizes
        :type window_list: list of integer

        :return: dictionary (string --> GeneralizePattern)
        :rtype:  dict
        """

        _nb_variables = ts.nb_variables()
        _hash = dict()
        _data_symbols = ts.data_symbols
        for _var_id in np.arange(_nb_variables):
            _set = set()
            _gen = self.pattern_generator(_data_symbols, _var_id, window_list)
            for slider in _gen:
                for pos, _pattern in enumerate(slider):
                    # pattern_key holds a "label:variable:pattern" structure
                    pattern_key = '%s:%s:%s' % (str(ts.label), str(_var_id), _pattern)
                    if pattern_key not in _hash:
                        _hash[pattern_key] = gp.GeneralizedPattern(p_min=_pattern, label=ts.label, variable=str(_var_id))
                    if _pattern not in _set:
                        _set.add(_pattern)
                        _hash[pattern_key].stats.new_example(ts.id)
                    _hash[pattern_key].stats.appears(ts.id, pos)
        return _hash

    def pattern_generator_multivar(self, windows, symbols, vars, no_gap = True, gap=0):
        """
        returns a list of list, where each contains the patterns related
        to a different window size (window_list) for a pair of variables.
        (currently only work with no gap)
        :param windows: a list of window sizes
        :param symbols: a string representation of the timeseries
        :param vars: the pair of variables
        :param no_gap: if a gap between the two timeseries should be considered
        :return: a list of list (string) that will enumerate all the patterns.
        """

        results = []
        for windows_list in windows:
            # Calculate when the slider should stop with/without gap
            if not no_gap:
                for i in range(len(vars)):
                    symbols[vars[i]] = symbols[vars[i]][sum(windows_list[0:i]) + gap - 1:]

                nb_windows = len(symbols[len(vars) - 1])
                var = len(windows_list) - 1
            else:
                nb_windows = len(symbols[vars[0]])
                var = 0

            # Count the bag of patterns
            window_patterns = []
            for i in range(nb_windows - windows_list[var] + 1):
                _str = ''
                for j in range(len(windows_list)):
                    _str += "".join([x for x in symbols[vars[j]][i:i + windows_list[j]]])
                window_patterns.append(_str)
            results.append(window_patterns)

        return results

    def compute_stats_multivar(self, ts, window_list, no_gap = True, gap=0):
        """
        Generate the bag of patterns statistics

        :param ts: a timeseries
        :type ts: TimeSeries
        :param window_list: a list of window sizes
        :type window_list: list of integer

        :return: dictionary (string --> GeneralizePattern)
        :rtype:  dict
        """
        nb_vars = ts.nb_variables()
        _hash = dict()
        data_symbols = ts.data_symbols
        for i,j in itertools.combinations(range(nb_vars), 2):
            _temp_str = str(i) + '-' + str(j)
            _set = set()
            _vars = [i, j]
            _gen = self.pattern_generator_multivar(window_list, data_symbols, _vars)
            for slider in _gen:
                for (pos, _pattern) in enumerate(slider):
                    pattern_key = '%s:%s:%s' % (str(ts.label), _temp_str, _pattern)
                    if pattern_key not in _hash:
                        _hash[pattern_key] = gp.GeneralizedPattern(p_min=_pattern, label=ts.label, variable=_temp_str)
                    if _pattern not in _set:
                        _set.add(_pattern)
                        _hash[pattern_key].stats.new_example(ts.id)
                    _hash[pattern_key].stats.appears(ts.id, pos)
        return _hash

    # Noise handling
    def noise_handling_pregeneralize(self, valid_patterns, black_patterns, intersection, rhm_dh):
        """
        Check all low level pattern if they satisfy the noise constraint
        :param valid_patterns: low level valid patterns
        :param black_patterns: low level non-valid patterns
        :param intersection: intersection of valid/black patterns (the patterns to check)
        :param rhm_dh: RHM data holder
        :return:
        """
        valid = set()

        #check the noise percentage in term of example coverage (satify also unbalanced dataset)
        for key in intersection:
            _statistic_0 = valid_patterns[key].stats
            _count_0 = _statistic_0.ex_count

            if _count_0 <= rhm_dh.threshold:
                continue

            _count_0 = float(_count_0) / float(rhm_dh.v_excount)

            _count_1 = float(black_patterns[key].stats.ex_count) / float(rhm_dh.nv_excount)

            _sum = _count_0 + _count_1

            temp_value = float(_count_0) / float(_sum)

            if temp_value >= rhm_dh.noise_p:
                valid.add(key)
        return valid

    def noise_handling_generalizing(self, pattern, rhm_dh, pattern_permutation):
        """
        handling noise for generalized patterns.
        different from the previous method in term of number of patterns to check.
        e.g. [AD]B --> we have to check patterns [AB, BB, CB, DB]
        :param pattern: pattern to check
        :type pattern: GeneralizedPattern
        :param rhm_dh: data holder, hold the noise, threshold, ...
        :type rhm_dh: RHMDataHolder
        :param pattern_permutation: a list of low level patterns that fall in the area of pattern
        :return: True if the pattern satisfy the noise percentage
        """

        temp_set_valid = set()
        temp_set_black = set()
        for pat in pattern_permutation:
            if pat in rhm_dh.valid_set:
                pattern.stats.merge(rhm_dh.valid_set[pat].stats)
                temp_set_valid.update(rhm_dh.valid_set[pat].stats.example_set)

            if pat in rhm_dh.black_set:
                temp_set_black.update(rhm_dh.black_set[pat].stats.example_set)
                pattern.stats.black_merge(rhm_dh.black_set[pat].stats)


        if (len(temp_set_black) == 0):
            return True
        _count_1 = float(len(temp_set_black)) / float(rhm_dh.nv_excount)

        if float(len(temp_set_valid)) <= float(rhm_dh.threshold):
            return False

        _count_0 = float(len(temp_set_valid)) / float(rhm_dh.v_excount)
        _sum = _count_0 + _count_1

        temp_value = float(_count_0) / float(_sum)

        if temp_value >= rhm_dh.noise_p:
            return True

        return False

    # Building the pos:letter dictionary
    def build_position_dictionary(self, patterns):
        """
        Build a dictionary of "pos:letter" --> list of patterns that has that letter at that position.
        e.g. 1:A --> ['BAC', 'AAC', 'CAA', ...]
        handle different size patterns.
        :param patterns: low level validated patterns
        :return: the dictionary
        """

        pat_pos_dict = dict()

        if patterns is None:
            return pat_pos_dict

        for pat in patterns:
            pat_len = len(pat)
            if pat_len not in pat_pos_dict:
                pat_pos_dict[pat_len] = [dict() for i in range(pat_len)]

            for i in range(pat_len):
                curr_struct = pat_pos_dict[pat_len][i]
                ch = pat[i]
                if ch not in curr_struct:
                    curr_struct[ch] = set()
                if pat not in curr_struct[ch]:
                    curr_struct[ch].add(pat)
        return pat_pos_dict

    def pattern_to_pos_keys(self, pattern):
        keys = []
        for i in range(len(pattern)):
            pos_set = set()
            min_ch = pattern.p_min[i]
            max_ch = pattern.p_max[i]
            temp_ch = min_ch
            while temp_ch <= max_ch:
                pos_set.add(temp_ch)
                temp_ch = chr(ord(temp_ch) + 1)
            keys.append(pos_set)
        return keys

    # Wild card
    def pattern_to_wild(self, pattern, bins, curr_var, pat_length):
        """
        generate the wild candidates, e.g. ABCD --> ['A.CD', 'AB.D', 'A..D']
        :param pattern: initial pattern
        :param bins: list of variables bins
        :param curr_var: the pattern variable
        :param pat_length: pattern length (window-length)
        :return: list of candidate patterns (string)
        """

        wilds = set()
        w = '[A' + rh.to_chr(bins[curr_var[0]] - 1) + ']'
        _range = None
        suffix = 0
        if pat_length < 3:
            wilds.add(pattern)
            return wilds

        if pat_length > 4:
            div = int(pat_length / 2)

            if pat_length % 2 == 0:
                _range = range(div - 2, div + 2)
                wilds.add(pattern[0:div - 2])
                suffix = pattern[div + 2:]
            else:
                _range = range(div - 1, div + 2)
                wilds.add(pattern[0:div - 1])
                suffix = pattern[div + 2:]
        elif pat_length == 3:
            _range = range(1, 2)
            wilds.add(pattern[0])
            suffix = pattern[2]
        else:
            _range = range(1, 3)
            wilds.add(pattern[0])
            suffix = pattern[3]

        for ind in _range:
            ch = pattern[ind]
            temp_wilds = []
            for pat in wilds:
                temp_wilds.append(pat + ch)
                temp_wilds.append(pat + '.')
            wilds = temp_wilds

        wilds = [wild + suffix for wild in wilds]
        results = []
        if len(curr_var) > 1:
            sec_loc = int(pat_length / 2)
            w_1 = [wild[:sec_loc] for wild in wilds]
            w_2 = [wild[sec_loc:] for wild in wilds]
            w_1 = [wild.replace('.', w) for wild in w_1]
            w2 = '[A' + rh.to_chr(bins[curr_var[1]] - 1) + ']'
            w_2 = [wild.replace('.', w2) for wild in w_2]
            for i in range(len(w_1)):
                results.append(w_1[i] + w_2[i])
        else:
            results = [wild.replace('.', w) for wild in wilds]

        return results

    # Best one step
    def expand_ranges(self, pat, upper_bound, rhm_dh, pos_dict):
        """
        Try to expand a pattern one step up and one step down,
        keeping at the end the most general pattern.
        :param pat: pattern to expand
        :param upper_bound: pattern discretizing bins (list since we might deal with pair of vars)
        :param rhm_dh: RHM data holder.
        :param pos_dict: dictionary of pos:letter --> list of patterns
        :return: the most generalized pattern.
        """

        current_pattern = self.strpat_to_pattern(pat)
        length = len(current_pattern)
        if len(upper_bound) > 1:
            windows_list = [int(length / 2), int(length / 2)]
        else:
            windows_list = [length]

        int_lower_bound = 0
        index = 0
        _range = 0

        if windows_list == None:
            windows_list = []
            windows_list.append(len(current_pattern.p_min))

        temp_pattern = copy.deepcopy(current_pattern)

        for window in windows_list:
            for pos in range(_range, _range + window):
                int_upper_bound = rh.to_int(upper_bound[index])
                best_range = 0

                #generate the list of candidates from the one step expanding method
                for shift in self.one_step_expand(current_pattern, pos, int_lower_bound, int_upper_bound):
                    #building the new candidate
                    temp_min = current_pattern.p_min[0:pos] + rh.to_chr(shift[0]) + current_pattern.p_min[pos+1:]
                    temp_max = current_pattern.p_max[0:pos] + rh.to_chr(shift[1]) + current_pattern.p_max[pos + 1:]
                    new = gp.GeneralizedPattern(temp_min, temp_max)

                    pos_keys = self.pattern_to_pos_keys(new)
                    post_dict_window = pos_dict[len(new)]
                    consid_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
                    permutation = set.union(*consid_pats)
                    for i in range(1, len(pos_keys)):
                        consid_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
                        consid_pats = set.union(*consid_pats)
                        permutation = permutation.intersection(consid_pats)

                    if(rhm_dh.noise_p < 0):
                        #not_black = new not in black_list
                        for pat in permutation:
                            if pat in rhm_dh.black_set:
                                not_black = False
                                break
                        else:
                            not_black = True
                    else:
                        not_black = self.noise_handling_generalizing(new, rhm_dh, permutation)

                    _shift_ar = shift[1] - shift[0]
                    if not_black and best_range < _shift_ar:
                        best_range = _shift_ar
                        temp_pattern = new

                current_pattern = temp_pattern

            index += 1
            _range += window

        return current_pattern

    def one_step_expand(self, pattern, pos, lower, upper):
        """
        Generate the set of one-step extended patterns depending for a specific position
        :param pattern: original pattern
        :param pos: position to extended in the pattern
        :param lower: lower range bin
        :param upper: upper range bin
        :return: list of candidates patterns
        """
        expandies = []
        pos_min = pattern.p_min[pos]
        pos_min_int = rh.to_int(pos_min)
        pos_max = pattern.p_max[pos]
        pos_max_int = rh.to_int(pos_max)

        if pos_min_int == lower:
            down_range = 0
        else:
            down_range = pos_min_int - 1
        if pos_max_int == upper:
            up_range = upper
        else:
            up_range = pos_max_int + 1
        for shift in itertools.product(range(down_range, pos_min_int + 1), range(pos_max_int, up_range + 1)):
            expandies.append(shift)
        return expandies

    # One step two directions
    def expand_one_step(self, pattern, upper_bound, curr_var):
        """
        Generate all possible one step up/down patterns from a given pattern.
        e.g. [BD] --> [AD], [BE], [AE]
        :param pattern: pattern to expand
        :param upper_bound: bins
        :param curr_var: variable of the pattern
        :return:
        """
        gpat = self.strpat_to_pattern(pattern)
        if len(curr_var) > 1:
            windows_size = len(gpat.p_min) / 2
        else:
            windows_size = len(gpat.p_min)
        candidates = []
        gc.enable()
        for pos in range(len(gpat)):
            temp_cand = []
            if pos >= windows_size:
                temp_cand = self.one_step_expand_str(gpat, pos, upper_bound[curr_var[1]] - 1)
            else:
                temp_cand = self.one_step_expand_str(gpat, pos, upper_bound[curr_var[0]] - 1)
            if len(candidates) == 0:
                candidates = copy.deepcopy(temp_cand)
            else:
                concat = []
                for pat_cn in candidates:
                    for pat_tmp in temp_cand:
                        concat.append(pat_cn + pat_tmp)
                candidates = concat
            gc.collect()
        return candidates

    def one_step_expand_str(self, pattern, pos, upper):
        """
        Generate the set of one-step extended patterns depending for a specific position
        :param pattern: original pattern
        :param pos: position to extended in the pattern
        :param lower: lower range bin
        :param upper: upper range bin
        :return: list of candidates patterns
        """
        expandies = []
        lower_bound = 0
        pos_min = pattern.p_min[pos]
        pos_min_int = rh.to_int(pos_min)
        pos_max = pattern.p_max[pos]
        pos_max_int = rh.to_int(pos_max)

        if pos_min_int == lower_bound:
            down_range = 0
        else:
            down_range = pos_min_int - 1
        if pos_max_int == upper:
            up_range = upper
        else:
            up_range = pos_max_int + 1
        for shift in itertools.product(range(down_range, pos_min_int + 1), range(pos_max_int, up_range + 1)):
            if shift[0] == shift[1]:
                expandies.append(rh.to_chr(shift[0]))
            else:
                expandies.append('[' + rh.to_chr(shift[0]) + rh.to_chr(shift[1]) + ']')
        return expandies

    def strpat_to_pattern(self, str_pat):
        """
        Create a GeneralizedPattern from a string representation of a pattern
        :param str_pat: the string representation
        :return: GeneralizedPattern
        """
        p_min = ''
        p_max = ''
        check = False
        ind = 0
        while ind < len(str_pat):
            ch = str_pat[ind]
            if ch == '[':
                check = True
                ind += 1
                continue
            if ch == ']':
                ind += 1
                continue
            if check:
                p_min += ch
                ind += 1
                p_max += str_pat[ind]
                check = False
            else:
                p_min += ch
                p_max += ch
            ind += 1

        return gp.GeneralizedPattern(p_min, p_max)

    # Other different operations
    def remove_duplicates(self, learned_set):
        """
        remove the duplicate patterns from the set of learned patterns.
        duplicate doesn't only mean the exact same pattern but also
        it remove the patterns that are included in some other patterns.
        e.g. [BD] and [AD] --> we remove [BD]
        :param learned_set:
        :return:
        """
        set_copy = copy.deepcopy(learned_set)
        for pat1, pat2 in itertools.combinations(set_copy, 2):
            if pat1 not in learned_set or pat2 not in learned_set:
                continue
            temp_pat_1 = learned_set[pat1]
            temp_pat_2 = learned_set[pat2]
            if len(temp_pat_1) != len(temp_pat_2):
                continue

            dist = temp_pat_1.dist(temp_pat_2)
            if dist == 0:
                comp = temp_pat_1.compare_coverage(temp_pat_2)
                if comp == 0:
                    continue
                elif comp == 1:
                    del learned_set[pat2]
                else:
                    del learned_set[pat1]

        return learned_set

    def aggregate_patterns(self, pattern, rhm_dh, pos_dict):
        """
        From a string representation of a pattern, build its GeneralizedPattern with
        all its related statistics
        :param pattern: a string representation of a pattern
        :param rhm_dh: RHM data holder
        :return: a GeneralizedPattern
        """
        new = self.strpat_to_pattern(pattern)
        pos_keys = self.pattern_to_pos_keys(new)
        post_dict_window = pos_dict[len(new)]
        consid_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
        permutation = set.union(*consid_pats)
        for i in range(1, len(pos_keys)):
            consid_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
            consid_pats = set.union(*consid_pats)
            permutation = permutation.intersection(consid_pats)

        if int(rhm_dh.noise_p) == 1:
            for pat in permutation:
                if pat in rhm_dh.black_set:
                    not_black = False
                    break
            else:
                not_black = True
        else:
            not_black = self.noise_handling_generalizing(new, rhm_dh, permutation)

        if not_black:
            return new

        return []

    def remove_rotation(self, p_set):
        """
        Remove the patterns that are a rotation of other patterns,
        e.g. ABAB/BABA, AA/AAB.
        This function takes into account the coverage of each pattern and select the most simple one.
        :param p_set: list of validated patterns.
        :return: a filtered list of patterns
        """
        set_copy = copy.deepcopy(set(p_set.keys()))
        for pat1, pat2 in itertools.combinations(sorted(set_copy), 2):
            if pat1 in p_set and pat2 in p_set:
                set_1 = p_set[pat1].stats.example_set
                set_2 = p_set[pat2].stats.example_set
                if len(p_set[pat1]) == len(p_set[pat2]) and pat1 in pat2*2:
                    if set_1.issubset(set_2):
                        del p_set[pat1]
                    elif set_2.issubset(set_1):
                        del p_set[pat2]
                elif len(p_set[pat1]) > len(p_set[pat2]) and pat2 in pat1:
                    if set_1.issubset(set_2):
                        del p_set[pat1]
                else:
                    if len(p_set[pat1]) < len(p_set[pat2]) and set_2.issubset(set_1):
                        del p_set[pat2]
        return p_set

    """
    def remove_rotation(p_set):
        set_copy = copy.deepcopy(set(p_set.keys()))
        for pat1, pat2 in itertools.combinations(sorted(set_copy), 2):
            if pat1 in p_set and pat2 in p_set:
                set_1 = p_set[pat1].stats.example_set
                set_2 = p_set[pat2].stats.example_set
                if len(p_set[pat1]) == len(p_set[pat2]) and pat1 in pat2*2:
                    if set_1.issubset(set_2):
                        del p_set[pat1]
                    elif set_2.issubset(set_1):
                        del p_set[pat2]
                elif len(p_set[pat1]) > len(p_set[pat2]) and pat2 in pat1 and set_1.issubset(set_2):
                    del p_set[pat1]
                elif len(p_set[pat1]) < len(p_set[pat2]) and set_2.issubset(set_1) and pat1 in pat2 :
                    del p_set[pat2]
        return p_set
    """

    # Tiling
    def tiling(self, rhm_dh, data_length):
        """
        From a set of valid patterns, select a subset of patterns that cover the same amount of ts.
        :param rhm_dh: RHM data holder
        :return: subset of patterns if applicable, the same set if not
        """
        res = set()
        remains = copy.deepcopy(set(rhm_dh.valid_set.keys()))
        covered_set = set()
        best_pat = None
        best_set = set()
        for pat in sorted(rhm_dh.valid_set):
            pat_set = rhm_dh.valid_set[pat].stats.example_set
            covered_set.update(pat_set)
            if len(pat_set):
                best_pat = pat
                best_set = rhm_dh.valid_set[best_pat].stats.example_set

        # Initializing the weights
        tiling_weight = [1] * data_length
        for ex in best_set:
            tiling_weight[ex] += 1
        covered_set = covered_set.difference(best_set)
        remains.remove(best_pat)
        res.add(best_pat)

        # Updating the weights after each cycle
        while len(covered_set) > 0:
            max_val = -10000
            best_set = set()
            for pat in sorted(remains):
                best_val = 0
                pat_set = rhm_dh.valid_set[pat].stats.example_set
                black_set = rhm_dh.valid_set[pat].stats.example_bset
                for ex in pat_set:
                    best_val += 1 / tiling_weight[ex]
                for ex in black_set:
                    best_val -= 1 / tiling_weight[ex]
                if best_val > max_val or (best_val == max_val and len(pat_set) > len(best_set)):
                    max_val = best_val
                    best_pat = pat
                    best_set = pat_set
            covered_set = covered_set.difference(best_set)
            remains.remove(best_pat)
            res.add(best_pat)

        """
        # Extension on the greedy tiling:
        # keep all the patterns that cover a certain percentage of examples
        for key in rhm_dh.valid_set:
            if len(rhm_dh.valid_set[key].stats.example_set) >= rhm_dh.conf_threshold:
                res.add(key)
        """

        return res

    def greedy_tiling(self, rhm_dh):
        """
        does a gready tiling with pattern_set. We want to tile the
        set of covered example with the minimum number of patterns

        :pattern_set: a set of pattern
        :type pattern_set: iterable over `GeneralizedPattern`
        :return: the set of selected patterns (as indices), and the tiling set (as a set)
        :rtype: a tuple (array of indices, set)
        """
        res = set()
        best_set = set()
        remains = sorted(set(rhm_dh.valid_set.keys()))
        tiling = set()

        while len(remains) > 0:
            best_set.clear()
            best_key = None
            # looking for the pattern with the covering set that has the largest
            # number of elements not in tiling
            for key in remains:
                current_set = rhm_dh.valid_set[key].stats.example_set
                # len of current set is smaller than the current best -> cannot improve
                if len(current_set) < len(best_set):
                    continue
                if best_key is not None  \
                       and len(key) < len(best_key) \
                       and len(current_set.difference(tiling)) == len(best_set):
                    best_set = current_set.difference(tiling)
                    best_key = key

                temp_set = current_set.difference(tiling)
                if len(temp_set) > len(best_set):
                    best_set = temp_set
                    best_key = key

            if best_key is not None:
                remains.remove(best_key)
                tiling.update(best_set)
                res.add(best_key)
            else:
                # nobody in remains contains element not in tiling -> stop
                break

        """
        #Extension on the greedy tiling:
        #keep all the patterns that cover a certain percentage of examples
        for key in rhm_dh.valid_set:
            if len(rhm_dh.valid_set[key].stats.example_set) >= rhm_dh.conf_threshold:
                res.add(key)
        """
        return res

    """
    def validate_pattern(pattern, rhm_dh, pos_dict, curr_vars):

        new = strpat_to_pattern(pattern)
        pos_keys = pattern_to_pos_keys(new)
        post_dict_window = pos_dict[len(new)]
        consid_pats = [set(v) for k, v in post_dict_window[0].items() if k in pos_keys[0]]
        permutation = set.union(*consid_pats)
        for i in range(1, len(pos_keys)):
            consid_pats = [set(v) for k, v in post_dict_window[i].items() if k in pos_keys[i]]
            consid_pats = set.union(*consid_pats)
            permutation = permutation.intersection(consid_pats)

        candidate_list = expand_one_step(pattern, rhm_dh.bins, curr_vars)
        candidate_list.remove(pattern)
        for new_pat in candidate_list:
            continue

        return
    """

    # Data preparation
    def rhm_data_prepare(self, input, train=True):
        # Data reader
        _ls_builder = tsls.LearningSetBuilder()
        _ls_builder.set_input_format(self.config.format)
        _Data = _ls_builder.build(input, self.config.derivative, self.config.delimter)

        if train:
            self.train_data_len = len(_Data)
            names = dict()
            if self.config.names:
                for i, j in enumerate(self.config.names):
                    names[i] = j
                if self.config.pair_windows:
                    for i, j in itertools.combinations(self.config.names, 2):
                        self.config.names.append(i + '-' + j)

                # visu
                if self.config.vis:
                    self.visu_raw_metadata['variables'] = self.config.names
                    self.visu_disc_metadata['variables'] = self.config.names
            self.names = names

        if len(self.config.columns) > 0:
            _Data.subset_variable(self.config.columns)

        if train:
            for i in range(len(self.config.columns)):
                self.all_variables.append(str(i))
            if self.config.pair_windows:
                for i, j in itertools.combinations(range(len(self.config.columns)), 2):
                    self.all_variables.append(str(i) + '-' + str(j))

        if self.config.data_subset:
            interval = [x for x in self.config.data_subset.split(":")]
            interval[1] = None if interval[1] == "None" else int(interval[1])
            interval[0] = int(interval[0])
            _Data.set_time_len(interval)

        if self.config.filter:
            all_filters = self.config.filter.split(',')
            for _filter in all_filters:
                _ct = _filter.split(':')
                ls = True
                if 'gr' in _ct:
                    ls = False
                _Data.filter_variable(int(_ct[0]), float(_ct[1]), ls)

        # visu
        if self.config.vis:
            if 'ucr' in self.config.format:
                ucrd = True
            else:
                ucrd = False
            self.visu_raw['raw'] = rh.data_to_vis(_Data, ucrd)
            min_length = _Data.series_min_length()
            max_length = _Data.series_max_length()
            self.visu_raw_metadata['min_x'] = min_length
            self.visu_raw_metadata['max_x'] = max_length
            self.visu_disc_metadata['min_x'] = min_length
            self.visu_disc_metadata['max_x'] = max_length

        return _Data

    # RHM Learn
    def rhm_train(self, _Data):
        if self.config.labels is not None:
            all_labels = set(_Data.get_labels())
            if not self.config.labels.issubset(all_labels):
                raise ValueError("Input labels are not dataset labels")
            _Data.cut_labels(self.config.labels)
        else:
            self.config.labels = [x for x in set(_Data.get_labels())]

        # Copy the data
        _Data_disc = deepcopy(_Data)

        # Discretizing the data
        _discretizer = ed.Discretizer()
        _discretizer.compute_breakpoints(_Data_disc, self.config.bins)

        conf = SparkConf().setMaster("spark://sami-HP-EliteBook-840-G2:7077").setAppName("Hello Spark").set(
            "spark.cores.max", 4).set("spark.executor.memory", "5g")
        sc = SparkContext(conf=conf, pyFiles=['/home/sami/Desktop/Ikats-LIG-StandAlone/ikats.zip'])

        # conf = SparkConf().setMaster("spark://racer:7077").setAppName("Hello Spark").set("spark.cores.max", 15).set("spark.executor.memory", "5g")
        # sc = SparkContext(conf=conf, pyFiles=['/home/ama/alkhousa/Ikats-LIG-StandAlone-SPARK/ikats.zip'])

        # conf = SparkConf().setMaster("local").setAppName("Hello Spark")
        # sc = SparkContext(conf=conf)

        sc.setLogLevel("ERROR")
        _data_spark = sc.parallelize(_Data_disc)
        _data_spark_disc = _data_spark.map(_discretizer.discretize_ts)

        # visu
        if self.config.vis:
            self.visu_disc['raw'] = rh.data_disc_to_vis(_Data_disc)
            if len(self.config.columns) > 1:
                if 'ucr' in self.config.format:
                    self.visu_raw_metadata['min_y'] = [x[0] for x in _discretizer.min_values]
                    self.visu_raw_metadata['max_y'] = [x[0] for x in _discretizer.max_values]
                else:
                    self.visu_raw_metadata['min_y'] = [x for x in _discretizer.min_values]
                    self.visu_raw_metadata['max_y'] = [x for x in _discretizer.max_values]
                self.visu_raw_metadata['break_points'] = [list(x) for x in _discretizer.breakpoints]
            else:
                self.visu_raw_metadata['min_y'] = _discretizer.min_values[0]
                self.visu_raw_metadata['max_y'] = _discretizer.max_values[0]
                self.visu_raw_metadata['break_points'] = list(_discretizer.breakpoints[0])

            self.visu_disc_metadata['min_y'] = [rh.to_chr(x) for x in [0] * len(self.config.bins)]
            self.visu_disc_metadata['max_y'] = [rh.to_chr(x - 1) for x in self.config.bins]
            self.visu_disc_metadata['break_points'] = []
            for b in self.config.bins:
                lst = []
                for i in range(0, b):
                    lst.append(rh.to_chr(i))
                self.visu_disc_metadata['break_points'].append(lst)

            self.visu_raw_metadata['labels'] = self.config.labels
            self.visu_disc_metadata['labels'] = self.config.labels
            self.visu_raw['meta_data'] = self.visu_raw_metadata
            self.visu_disc['meta_data'] = self.visu_disc_metadata

        labels_examples = dict()
        # Count the coverage of each label

        for label in self.config.labels:
            self.labels_coverage[label] = len([x for x in _Data_disc.get_labels() if x == label])
            labels_examples[label] = {i for i in range(len(_Data_disc)) if _Data_disc.get_label(i) == label}

        # Compute patterns for each window (one variable)
        _stats = _data_spark_disc.map(lambda ts: self.compute_stats(ts, self.config.windows))
        _stats = _stats.flatMap(lambda pat: pat.items())

        # Compute patterns for each windows (pair of variables)
        windows_list = []
        _stats_pairs = None
        if self.config.pair_windows:
            for w in self.config.pair_windows:
                windows_list.append((int(w / 2), int(w / 2)))
            _stats_pairs = _data_spark_disc.map(lambda ts: self.compute_stats_multivar(ts, windows_list))
            _stats_pairs = _stats_pairs.flatMap(lambda pat: pat.items())

        if _stats_pairs:
            _stats = _stats.union(_stats_pairs)
        _stats = _stats.reduceByKey(self.merge)
        _stats = _stats.filter(lambda pat: self.filter_on_coverage(pat[1], self.labels_coverage,
                                                                  self.config.cov_threshold))
        _stats = _stats.map(lambda pat: pat[1])
        _stats.persist()

        all_valid = dict()
        all_black = dict()

        # for each label, learn their relative patterns
        for i in sorted(self.config.labels):
            _labels = []
            _labels.append(str(i))
            _labels.extend([str(j) for j in self.config.labels if i != j])
            c0 = _labels[0]
            c1 = _labels[1:]
            v_excount = self.labels_coverage[c0]
            nv_excount = len(_Data) - v_excount
            threshold = round(v_excount * self.config.cov_threshold)
            conf_threshold = round(v_excount * self.config.conf_threshold)
            nthreshold = round(nv_excount * self.config.cov_threshold)

            # RHMDataHolder initialization, this was done to reduce the number of parameters
            rhm_dh = rdh.RHMDataHolder(c0, c1, labels_examples, v_excount, nv_excount, self.config.bins, None, None,
                                       self.config.windows, self.config.pair_windows, self.config.noise,
                                       threshold, conf_threshold)

            # for each variable, learn their relative patterns
            for var in self.all_variables:
                valid = dict()
                black_dict = dict()

                # Compute the non-valid patterns
                black_patterns = [pat for pat in _stats.collect() if pat.label != c0 and pat.variable == var]
                for g_pat in black_patterns:
                    str_pat = str(g_pat)
                    if (str_pat in black_dict):
                        black_dict[str_pat].stats.merge(deepcopy(g_pat.stats))
                    else:
                        black_dict[str_pat] = deepcopy(g_pat)

                black_dict = {p: black_dict[p] for p in black_dict if black_dict[p].stats.ex_count > nthreshold}
                black_list_keys = set(black_dict.keys())

                # for printing
                curr_vars = [int(x) for x in var.split('-')]
                if len(curr_vars) > 1:
                    _temp_lab_var = c0 + ':' + self.names[curr_vars[0]] + '-' + self.names[curr_vars[1]]
                else:
                    _temp_lab_var = c0 + ':' + self.names[curr_vars[0]]

                # Compute the valid-patterns
                valid_str = c0 + ':' + var
                pattern_array = {str(pat): pat for pat in _stats.collect() if pat.label == c0 and pat.variable == var}
                valid.update({p: pattern_array[p] for p in pattern_array if p not in black_list_keys})
                intersection = set(pattern_array.keys()).intersection(black_list_keys)

                # Handling noise
                if rhm_dh.noise_p >= 0 and rhm_dh.noise_p <= 1:
                    noisy_valid_pattern = self.noise_handling_pregeneralize(pattern_array, black_dict, intersection, rhm_dh)
                    for key in sorted(noisy_valid_pattern):
                        valid[key] = pattern_array[key]

                rhm_dh.valid_set = valid
                rhm_dh.black_set = black_dict
                # End of handling noise

                temp_set = set(valid.keys())
                temp_set.update(set(black_dict.keys()))
                pos_dict = self.build_position_dictionary(temp_set)

                # Generating wild-patterns
                _spark_wild = sc.parallelize(rhm_dh.valid_set.keys())
                _spark_wild = _spark_wild.flatMap(lambda pat: self.pattern_to_wild(pat, rhm_dh.bins,
                                                                                  curr_vars, len(rhm_dh.valid_set[pat])))
                _spark_wild = _spark_wild.distinct()
                _spark_wild = _spark_wild.map(lambda pat: self.aggregate_patterns(pat, rhm_dh, pos_dict))
                _spark_wild = _spark_wild.filter(lambda pat: pat)

                valid = {str(pat): pat for pat in _spark_wild.collect()}
                valid = self.remove_duplicates(valid)

                _spark_expand = sc.parallelize(valid.keys())

                # Expanding patterns
                """
                _spark_expand = _spark_expand.flatMap(lambda pat: self.expand_one_step(pat, rhm_dh.bins, curr_vars))
                _spark_expand = _spark_expand.distinct()
                _spark_expand = _spark_expand.map(lambda pat: self.aggregate_patterns(pat, rhm_dh, pos_dict))
                _spark_expand = _spark_expand.filter(lambda pat: pat)
                """

                _dbins = var.split('-')
                dbins = [rh.to_chr(self.config.bins[int(i)] - 1) for i in _dbins]
                _spark_expand = _spark_expand.map(lambda pat: self.expand_ranges(pat, dbins, rhm_dh, pos_dict))

                expand_valid = {str(pat): pat for pat in _spark_expand.collect()}
                expand_valid = self.remove_duplicates(expand_valid)
                valid.update(expand_valid)
                valid = self.remove_duplicates(valid)
                valid = self.remove_rotation(valid)
                rhm_dh.valid_set = valid

                if len(valid) > 0:
                    print(_temp_lab_var)
                    # indices = self.tiling(rhm_dh, self.train_data_len)
                    indices = self.greedy_tiling(rhm_dh)
                    valid = {k: valid[k] for k in indices}

                    ex_set_var = set()
                    for pat in sorted(valid):
                        ex_set_var.update(valid[pat].stats.example_set)
                        string_pat = rh.pattern_to_re2(pat, rh.to_chr(self.config.bins[int(curr_vars[0])] - 1))
                        print(string_pat, sorted(valid[pat].stats.example_set))
                    print('all_valid:  %s/%s' % (len(ex_set_var), v_excount))
                    print()

                if self.config.vis:
                    for pat in valid:
                        vis_pat = dict()
                        print_pat = rh.pattern_to_re2(pat, rh.to_chr(self.config.bins[int(curr_vars[0])] - 1))
                        vis_pat['key'] = print_pat
                        if '-' in var:
                            _pat_length = len(valid[pat].p_min) / 2
                        else:
                            _pat_length = len(valid[pat].p_min)
                        vis_pat['length'] = _pat_length
                        if '-' in var:
                            print_pat = rh.pattern_to_re(print_pat, len(valid[pat].p_min))
                        vis_pat['regex'] = print_pat
                        vis_pat['class'] = c0
                        vis_pat['variable'] = _temp_lab_var.split(':')[1]
                        locations = dict()
                        for ex in valid[pat].stats.example_set:
                            # locations['TS' + airbus_names[ex]] = sorted(list(valid[pat].stats.example_pos_set(ex)))
                            locations['TS' + str(ex)] = sorted(list(valid[pat].stats.example_pos_set(ex)))
                        for ex in valid[pat].stats.example_bset:
                            # locations['TS' + airbus_names[ex]] = sorted(list(valid[pat].stats.example_pos_bset(ex)))
                            locations['TS' + str(ex)] = sorted(list(valid[pat].stats.example_pos_bset(ex)))
                        vis_pat['locations'] = locations
                        self.visu_pattern[print_pat] = vis_pat

                all_valid[valid_str] = valid
                all_black[valid_str] = black_dict

        if self.config.vis:
            with open(self.config.output + '_raw.json', 'w') as file:
                json.dump(self.visu_raw, file)
            with open(self.config.output + '_disc.json', 'w') as file:
                json.dump(self.visu_disc, file)
            with open(self.config.output + '_pattern.json', 'w') as file:
                json.dump(self.visu_pattern, file)

        self.valid_patterns = all_valid
        self.black_patterns = all_black

        return (self.valid_patterns, self.black_patterns)

    def rhm_train_ikats(self, tslist=None, bins=None, labels=None, noise=1.0, cov_threshold=0.05,
                        windows=None, generalizer=False, conf_threshold=1.0):

        vis = True
        output = '/home/ikats/airbus'

        if labels is not None:
            all_labels = set(tslist.get_labels())
            if not labels.issubset(all_labels):
                raise ValueError("Input labels are not dataset labels")
            tslist.cut_labels(labels)
        else:
            labels = [x for x in set(tslist.get_labels())]

        # Copy the data
        _Data_disc = deepcopy(tslist)

        # Discretizing the data
        _discretizer = ed.Discretizer()
        _discretizer.compute_breakpoints(_Data_disc, bins)

        # Create or get a spark Context
        sc = ScManager.get()

        rdd_data_disc = sc.parallelize(_Data_disc, 32)

        # INPUT :  [item(TimeSeries)]
        # OUTPUT : [item(TimeSeries)]
        # PROCESS : Discretize each TS and generate its string representation
        rdd_data_disc = rdd_data_disc.map(_discretizer.discretize_ts)

        """
        # visu
        if vis:
            self.visu_disc['raw'] = rh.data_disc_to_vis(_Data_disc)
            if len(self.config.columns) > 1:
                if 'ucr' in self.config.format:
                    self.visu_raw_metadata['min_y'] = [x[0] for x in _discretizer.min_values]
                    self.visu_raw_metadata['max_y'] = [x[0] for x in _discretizer.max_values]
                else:
                    self.visu_raw_metadata['min_y'] = [x for x in _discretizer.min_values]
                    self.visu_raw_metadata['max_y'] = [x for x in _discretizer.max_values]
                self.visu_raw_metadata['break_points'] = [list(x) for x in _discretizer.breakpoints]
            else:
                self.visu_raw_metadata['min_y'] = _discretizer.min_values[0]
                self.visu_raw_metadata['max_y'] = _discretizer.max_values[0]
                self.visu_raw_metadata['break_points'] = list(_discretizer.breakpoints[0])

            self.visu_disc_metadata['min_y'] = [rh.to_chr(x) for x in [0] * len(self.config.bins)]
            self.visu_disc_metadata['max_y'] = [rh.to_chr(x - 1) for x in self.config.bins]
            self.visu_disc_metadata['break_points'] = []
            for b in self.config.bins:
                lst = []
                for i in range(0, b):
                    lst.append(rh.to_chr(i))
                self.visu_disc_metadata['break_points'].append(lst)

            self.visu_raw_metadata['labels'] = self.config.labels
            self.visu_disc_metadata['labels'] = self.config.labels
            self.visu_raw['meta_data'] = self.visu_raw_metadata
            self.visu_disc['meta_data'] = self.visu_disc_metadata
        """

        labels_examples = dict()
        # Count the coverage of each label

        for label in labels:
            self.labels_coverage[label] = len([x for x in _Data_disc.get_labels() if x == label])
            labels_examples[label] = {i for i in range(len(_Data_disc)) if _Data_disc.get_label(i) == label}

        # Compute patterns for each window (one variable)
        # INPUT :  [item(TimeSeries), list(int)]
        # OUTPUT : [dict (string --> GeneralizedPattern)]
        # PROCESS : Compute the bag of patterns for each TS
        rdd_stats = rdd_data_disc.map(lambda ts: self.compute_stats(ts, windows))
        print(rdd_stats.count())
        """
        rdd_stats = rdd_stats.flatMap(lambda pat: pat.items())
        rdd_stats = rdd_stats.reduceByKey(self.merge)
        rdd_stats = rdd_stats.filter(lambda pat: self.filter_on_coverage(pat[1], self.labels_coverage, cov_threshold))
        rdd_stats = rdd_stats.map(lambda pat: pat[1])
        rdd_stats.persist()

        all_valid = dict()
        all_black = dict()


        # for each label, learn their relative patterns
        for i in sorted(labels):
            if i == '0':
                continue
            _labels = []
            _labels.append(str(i))
            _labels.extend([str(j) for j in labels if i != j])
            c0 = _labels[0]
            c1 = _labels[1:]
            v_excount = self.labels_coverage[c0]
            nv_excount = len(tslist) - v_excount
            threshold = round(v_excount * cov_threshold)
            conf_threshold = round(v_excount * conf_threshold)
            nthreshold = round(nv_excount * cov_threshold)

            # RHMDataHolder initialization, this was done to reduce the number of parameters
            rhm_dh = rdh.RHMDataHolder(c0, c1, labels_examples, v_excount, nv_excount, bins, None, None,
                                       windows, None, noise, threshold, conf_threshold)

            # for each variable, learn their relative patterns
            for var in self.all_variables:
                valid = dict()
                black_dict = dict()

                # Compute the non-valid patterns
                black_patterns = [pat for pat in rdd_stats.collect() if pat.label != c0 and pat.variable == var]
                for g_pat in black_patterns:
                    str_pat = str(g_pat)
                    if (str_pat in black_dict):
                        black_dict[str_pat].stats.merge(deepcopy(g_pat.stats))
                    else:
                        black_dict[str_pat] = deepcopy(g_pat)

                black_dict = {p: black_dict[p] for p in black_dict if black_dict[p].stats.ex_count > nthreshold}
                black_list_keys = set(black_dict.keys())

                # for printing
                curr_vars = [int(x) for x in var.split('-')]
                if len(curr_vars) > 1:
                    _temp_lab_var = c0 + ':' + self.names[curr_vars[0]] + '-' + self.names[curr_vars[1]]
                else:
                    _temp_lab_var = c0 + ':' + self.names[curr_vars[0]]

                # Compute the valid-patterns
                valid_str = c0 + ':' + var
                pattern_array = {str(pat): pat for pat in rdd_stats.collect() if pat.label == c0 and pat.variable == var}
                valid.update({p: pattern_array[p] for p in pattern_array if p not in black_list_keys})
                intersection = set(pattern_array.keys()).intersection(black_list_keys)

                # Handling noise
                if rhm_dh.noise_p >= 0 and rhm_dh.noise_p <= 1:
                    noisy_valid_pattern = self.noise_handling_pregeneralize(pattern_array, black_dict, intersection, rhm_dh)
                    for key in sorted(noisy_valid_pattern):
                        valid[key] = pattern_array[key]

                rhm_dh.valid_set = valid
                rhm_dh.black_set = black_dict
                # End of handling noise

                temp_set = set(valid.keys())
                temp_set.update(set(black_dict.keys()))
                pos_dict = self.build_position_dictionary(temp_set)

                # Generating wild-patterns
                _spark_wild = sc.parallelize(rhm_dh.valid_set.keys())
                _spark_wild = _spark_wild.flatMap(lambda pat: self.pattern_to_wild(pat, rhm_dh.bins,
                                                                                  curr_vars, len(rhm_dh.valid_set[pat])))
                _spark_wild = _spark_wild.distinct()
                _spark_wild = _spark_wild.map(lambda pat: self.aggregate_patterns(pat, rhm_dh, pos_dict))
                _spark_wild = _spark_wild.filter(lambda pat: pat)

                valid = {str(pat): pat for pat in _spark_wild.collect()}
                valid = self.remove_duplicates(valid)

                _spark_expand = sc.parallelize(valid.keys())



                # Expanding patterns
                # _spark_expand = _spark_expand.flatMap(lambda pat: self.expand_one_step(pat, rhm_dh.bins, curr_vars))
                # _spark_expand = _spark_expand.distinct()
                # _spark_expand = _spark_expand.map(lambda pat: self.aggregate_patterns(pat, rhm_dh, pos_dict))
                # _spark_expand = _spark_expand.filter(lambda pat: pat)



                _dbins = var.split('-')
                dbins = [rh.to_chr(rhm_dh.bins[int(i)] - 1) for i in _dbins]
                _spark_expand = _spark_expand.map(lambda pat: self.expand_ranges(pat, dbins, rhm_dh, pos_dict))

                expand_valid = {str(pat): pat for pat in _spark_expand.collect()}
                expand_valid = self.remove_duplicates(expand_valid)
                valid.update(expand_valid)
                valid = self.remove_duplicates(valid)
                valid = self.remove_rotation(valid)
                rhm_dh.valid_set = valid

                if len(valid) > 0:
                    print(_temp_lab_var)
                    # indices = self.tiling(rhm_dh, self.train_data_len)
                    indices = self.greedy_tiling(rhm_dh)
                    valid = {k: valid[k] for k in indices}

                    ex_set_var = set()
                    for pat in sorted(valid):
                        ex_set_var.update(valid[pat].stats.example_set)
                        string_pat = rh.pattern_to_re2(pat, rh.to_chr(rhm_dh.bins[int(curr_vars[0])] - 1))
                        print(string_pat, sorted(valid[pat].stats.example_set))
                    print('all_valid:  %s/%s' % (len(ex_set_var), v_excount))
                    print()

                if vis:
                    for pat in valid:
                        vis_pat = dict()
                        print_pat = rh.pattern_to_re2(pat, rh.to_chr(rhm_dh.bins[int(curr_vars[0])] - 1))
                        vis_pat['key'] = print_pat
                        if '-' in var:
                            _pat_length = len(valid[pat].p_min) / 2
                        else:
                            _pat_length = len(valid[pat].p_min)
                        vis_pat['length'] = _pat_length
                        if '-' in var:
                            print_pat = rh.pattern_to_re(print_pat, len(valid[pat].p_min))
                        vis_pat['regex'] = print_pat
                        vis_pat['class'] = c0
                        vis_pat['variable'] = _temp_lab_var.split(':')[1]
                        locations = dict()
                        for ex in valid[pat].stats.example_set:
                            locations['TS' + str(ex)] = sorted(list(valid[pat].stats.example_pos_set(ex)))
                        for ex in valid[pat].stats.example_bset:
                            locations['TS' + str(ex)] = sorted(list(valid[pat].stats.example_pos_bset(ex)))
                        vis_pat['locations'] = locations
                        self.visu_pattern[print_pat] = vis_pat

                all_valid[valid_str] = valid
                all_black[valid_str] = black_dict

        if vis:
            #with open(output + '_raw.json', 'w') as file:
                #json.dump(self.visu_raw, file)
            #with open(output + '_disc.json', 'w') as file:
                #json.dump(self.visu_disc, file)
            with open(output + '_pattern.json', 'w') as file:
                json.dump(self.visu_pattern, file)

        self.valid_patterns = all_valid
        self.black_patterns = all_black
        rdd_stats.unpersist()
        sc.stop()

        return (self.valid_patterns, self.black_patterns)
        """

    # RHM run
    def run(self):
        data = self.rhm_data_prepare(self.config.input_file)
        self.rhm_train(data)
        gc.collect()
