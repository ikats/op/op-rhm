import numpy as np
from rhm.core import RHMDataHandler as rh


class Discretizer(object):
    """
    Class that discretize the data using equaly spaced  interval
    """

    def __init__(self, nb_bins=0):
        """
        Constructor of the Discretizer class.
        :param nb_bins: the number of bins to set (set to unset if not set)
        :type nb_bins: integer
        """
        self._max = None
        self._min = None
        self._breakpoints = None
        self._nb_bins = nb_bins
        self._histogram = None

    @property
    def max_values(self):
        """Get a list of maximum values of all variables"""
        return self._max

    @property
    def min_values(self):
        """Get a list of minimum values of all variables"""
        return self._min

    @property
    def breakpoints(self):
        """Getter: list of list, breakpoints values for each variable"""
        return self._breakpoints

    def compute_breakpoints(self, ts_set, bins_list):
        """
        Set up the configuration for the Discretizer
        BEFORE discretizing the values.

        :param ts_set: the input learning set
        :type ts_set: LearningSet
        :param bins_list: the number of bins to consider (size of nb_variables)
        :type bins_list: list of integers
        """

        if bins_list is None or len(bins_list) <= 0:
            raise ValueError("number of discretizing bins should be positive (got {}). ".format(bins_list))

        nb_vars = ts_set[0].nb_variables()
        if ts_set.nb_variables() != len(bins_list):
            raise ValueError("number of discretizing bins should be equal to the number of variables.")

        self._min = np.min(np.array([np.min(ts_set[i].data(), axis=0) for i in np.arange(len(ts_set))]), axis=0)
        self._max = np.max(np.array([np.max(ts_set[i].data(), axis=0) for i in np.arange(len(ts_set))]), axis=0)

        # Precision problem solved currently by adding 0.00000001 to the maximum value
        precision = 0.00000001
        self._breakpoints = np.array([np.arange(self._min[i],
                                                self._max[i],
                                                (self._max[i] + precision - self._min[i])/float(bins_list[i]))
                                      for i in np.arange(nb_vars)])

    def discretize(self, ts_set):
        """
        On place discretization of the input learning set.
        Stategy for discretization should have been defined already (see compute_breakpoints).

        :param ts_set: a learning set
        :type ts_set: LearningSet
        """

        for _ts in ts_set:
            self.discretize_ts(_ts)

    def discretize_ts(self, ts):
        """
        Discretize a given time series according to its breakpoints.
        :param ts: the input timeseries
        :type ts: TimeSeries
        """

        _data = ts.data()
        for j in np.arange(ts.nb_variables()):
            _data[:, j] = np.digitize(_data[:, j], self._breakpoints[j]) - 1

        # Generate a string representation of the discretized time series
        self.generate_symbols_rep(ts)

    def generate_symbols_rep(self, ts):
        """
        For each variable in each timeseries, generate the relative
        string representation according to its (variable) breakpoints
        :param ts: the input timeseries
        :type ts: TimeSeries
        """

        _nb_variables = ts.nb_variables()
        data_to_symbols = []
        _data = ts.data()
        for _var_id in np.arange(_nb_variables):
            data_to_symbols.append("".join([rh.to_chr(int(x)) for x in _data[:, _var_id]]))
        ts.data_symbols = data_to_symbols
