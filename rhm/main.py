import rhm.core.rhm as rhm
import rhm.core.RHMConfig as Cfg


def rhm_run(learningset_name=None,
            bins=None,
            labels=None,
            windows=None,
            purity=0.9,
            cov_threshold=0.2,
            generalizer=False,
            var_pair=False,
            conf_threshold=1.0):

    # Example
    bins = [bins]
    if labels:
        labels = set([x for x in labels.split(',')])
    else:
        labels = None

    windows = [int(x) for x in windows.split(',')]

    _rhm = rhm.RHM(Cfg.RHMConfig())

    data = _rhm.rhm_data_prepare(learningset_name, var_pair=var_pair)

    return _rhm.rhm_train_ikats(learningset=data,
                                bins=bins, labels=labels,
                                windows=windows,
                                purity=purity,
                                cov_threshold=cov_threshold,
                                generalizer=generalizer,
                                var_pair=var_pair,
                                conf_threshold=conf_threshold)
