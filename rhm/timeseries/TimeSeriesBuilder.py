import numpy as np
import time
import rhm.timeseries.TimeSeries as ts


class TimeSeriesBuilder(object):
    """ implements a builder """
    def __init__(self):
        """ initializes an empty builder

        default format for text format is : comma separated, comment are #, take all columns
        known format are txt and tdm (for temporal data manager)
        """
        self._input_file = None
        self._input_shape = {"comments": "#", 'delimiter': ','}
        self.__known_format = {"txt": self.build_from_txt,
                               "tdm": self.build_from_tdm}
        self.__builder = None
        self._random_state = None
        self.__tdm = None

    @property
    def input_shape(self):
        """getter for the input_shape parameter
        :return: the input shape parameter
        :rtype: dictionary
        """
        return self._input_shape

    def set_input_format(self, in_format, param=None):
        """ defines the input format that will be used

        :param in_format: describes the input format. Current available formats are:
          - "txt" with a csv format

          - "param" the parameter for the in_format.
            If in_format is tdm then the param format is supposed to be TemporalDataMgr object
            that will be used  to get the time serie.
            If in_format is "random", the param field is supposed to be a tuple of the form
            (seed,time_len,nb_variables).

        :type in_format: string
        """
        if in_format not in self.__known_format:
            raise ValueError("format {} not handle (yet ?). Known format are {}".
                             format(in_format, self.__known_format))
        self.__builder = self.__known_format[in_format]
        if in_format == "tdm":
            self.__tdm = param

        if in_format == "random":
            self._input_shape = {}
            self.set_missing_to_default_random_shape()

    def set_text_shape(self, loadtxt_params, update=True):
        """Defines the parameters used when input format is a text  file.

        :param loadtxt_params: defines the input format as a dictionnary that represents the
                         parameters of the np.loadtxt() function.
        :param update: True if the current input_shape has to be updated, False if it has to be
                     replaced
        :type loadtxt_params: dictionnary
        :type update: Boolean (default to True)
        """
        if update:
            self._input_shape.update(loadtxt_params)
        else:
            self._input_shape = loadtxt_params.copy()

    def set_missing_to_default_random_shape(self):
        """update self._input_shape with default values when missing.
           if 'seed' is missing, draw one at random and reset random_state
        """
        default = {"time_len": 50, "nb_variables": 2, "data_range": (0, 100)}
        for k in default.keys():
            if k not in self._input_shape:
                self._input_shape[k] = default[k]
        if "seed" not in self._input_shape \
               or self._input_shape["seed"] == "none" \
               or self._input_shape["seed"] is None:
            self._input_shape["seed"] = np.random.randint(0, int(time.time()))
            self._random_state = np.random.RandomState(self._input_shape["seed"])

    def set_random_shape(self, params, update=True):
        """
        Defines the parameters used when the input format is random (basically, the seed, ts shape)
        If params contains the 'seed' key, then the random generator is reseted with this seed.

        :param params: ts shape and seed
        :type params: dictionnary: keys are "seed", "time_len", "nb_variables" "data_range"
        :param update: (default True) if True, the current input_shape is updated else replaced.
        if missing key appears in the replaced dic, they are reset to default.
        :type update: Boolean
        """

        # memorize the seed : if changed by the user, reinitialize the random state
        if update:
            self._input_shape.update(params)
        else:
            self._input_shape = params.copy()
            self.set_missing_to_default_random_shape()

        if "seed" in params:
            if self._input_shape["seed"] == "none" \
                   or self._input_shape["seed"] is None:
                self._input_shape["seed"] = np.random.randint(0, int(time.time()))
            self._random_state = np.random.RandomState(self._input_shape["seed"])

    def build(self, input_file):
        """Loads the TS using the current context.

        :param input_file: the file from which we aim to load the data.
        :type input_file: Any type supported by the fname parameter of the np.loadtxt function
        :return: the result of the building
        :rtype: a time serie
        """
        return self.__builder(input_file)

    def build_from_tdm(self, ts_name):
        """ Loads the TS "ts_name" from the temporal data manager
        defined in the set_input_format method.

        This object should instantiate a get_ts_by_metric method that returns
        an two dimentional numpy array arr where arr[i] is of the form
        [ timestamps, valueVar1, valueVar2, ...]

        :param ts_name: a timeserie name (as provided by ts)
        :returns: a timeseries
        :rtype: timeseries

        """
        # build name from ts_uid:
        if isinstance(ts_name, str):
            _name = ts_name
        else:
            _name = "-".join(ts_name)
        c = np.concatenate(self.__tdm.get_ts(ts_name), axis=1)
        # just remove the timestamps from the serie and add the name.
        return ts.TimeSeries(c[:, 1:len(c[0]):2], name=_name)

    def build_from_txt(self, input_file):
        """ Load a TS from a txt file in csv format
        the loadtxt_params argument is a dictionnary that describes the format that conform to the
        numpy loadtxt function. The returned array should be such that r[:,0] is the timestamps
        and r[:,-1] are the values.
        """
        _loaded_array = np.loadtxt(fname=input_file, **self._input_shape)
        return ts.TimeSeries(values=_loaded_array, name=input_file)

    def build_from_json(self, values, name):
        return ts.TimeSeries(values=np.array(values), name=name)

    def build_from_list(self, values):
        """

        :param values: List of values
        :return:
        """
        return ts.TimeSeries(values=np.array(values, dtype=float))

    def build_from_array(self, values):
        return ts.TimeSeries(values=np.array(values, dtype=float))
