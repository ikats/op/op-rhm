import numpy as np


class TimeSeries(object):
    """

    Implements multivariate time series with uniform time stamps.

    .. warning:: The type of the element should be np.float64. If the values parameter is a list, \
    it will be converted to the correct type (there is duplication), else there is no duplication,\
    assuming the caller **will** provide an np.float64 ndarray.
    """

    def __init__(self, values=None, time_stamps=None, name=None, duplicate=False, data_symbols=None, id=None):

        """ creates a ts from values and time_stamps.

        :param values: the values of the timeseries
        :type values: either a list or a np.ndarray (dim 2)
        :param time_stamps: array of the timestamps (same format as values). \
        if not set, ts is supposed to be uniform. Non uniform timestamps are not yet managed.
        :param name: a name (or any id) for the ts
        :param duplicate: should we copy the data or just link them (default not copy).

        :warning:
            values are provided as a two dimensional np array of shape (#values,#variables).

            - If values is a python list or is a numpy array of dimension 1, it is transposed.

            - Univariate time serie with value 1,2,3 must be given as  [[1],[2],[3]] or [1, 2, 3].T

            - Array of the form [[1,2], [3,4], [5,6]] defines a bivariate time serie of time len 3,\
            one variable with values [1,3,5], the other one with values [2, 4, 6].

            - duplicate parameter is not consider if value is a python list (as they are copied \
               anyway) or a 1D np array.
        """

        if isinstance(values, list):
            self.__values = np.array([values], dtype=np.float64).T
        elif isinstance(values, np.ndarray) and len(values.shape) == 1:
            self.__values = np.array([values], np.float64).T
        else:
            self.__values = np.array(values, copy=duplicate)
        if time_stamps is not None:
            raise NotImplementedError("timeseries with non uniform timestamps are not yet managed")
        self.__name = name
        self.__data_symbols = data_symbols
        self._id = id

    def __str__(self):

        # Returns a printable version of the TS.
        return '\n'.join(["values:", str(self.__values), 'shape:', str(self.__values.shape)])

    def __getitem__(self, pos_x):

        """ returns the xth value, which can be the array of the values of the n variables"""
        return self.__values[pos_x]

    def data(self):

        """
        :return: the data itself (as a numpy array).
        :rtype: a numpy array
        """

        return self.__values

    def set_data(self, values):
        self.__values = values

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def data_symbols(self):

        """
        get a string representation (discretized) of the timeseries (all variables)
        :return: a list of string
        """

        return self.__data_symbols

    @data_symbols.setter
    def data_symbols(self, values):
        self.__data_symbols = values

    def time_len(self):

        """
        :return: the number of values in the first series
        :rtype: int
        """

        return self.__values.shape[0]

    def nb_variables(self):

        """
        :return: the number of variable  (ie 1 if univariate)
        :rtype: int
        """

        return self.__values.shape[1]

    @property
    def name(self):
        """getter for the name field.
        :return: the name
        :rtype: str
        """
        return self.__name

    @name.setter
    def name(self, name):
        """setter for the name field"""
        self.__name = name

    def set_time_len(self, interval):
        """
        Redefine the time interval of the time series. Done online
        :param interval: a tuple [min, max)
        :warning: interval is clipped to the [0, time_len] interval
        """
        if interval[0] is None or interval[0] < 0:
            _start = 0
        else:
            _start = interval[0]
        if interval[1] is None or interval[1] > self.time_len():
            _end = self.time_len()
        else:
            _end = interval[1]
        self.__values = self.__values[_start:_end, :]
        self.__values = np.ascontiguousarray(self.__values)


    def filter_variable_values_gr(self, variable, value):
        """ Filter the values of a variable based on input value (greater than)

        :param variabel: the variable of interest
        :param value: value to cut based upon
        """
        if len(self.__values.shape) > 2:
            _temp_rows = np.where(self.__values[:,variable,0] > value)
        else:
            _temp_rows = np.where(self.__values[:, variable] > value)
        self.__values = self.__values[_temp_rows]

    def filter_variable_values_ls(self, variable, value):
        """ Filter/keep the values of a variable based on input value (less than)

        :param variabel: the variable of interest
        :param value: value to cut based upon
        """
        if len(self.__values.shape) > 2:
            _temp_rows = np.where(self.__values[:,variable,0] < value)
        else:
            _temp_rows = np.where(self.__values[:, variable] < value)
        self.__values = self.__values[_temp_rows]


    def subset_variables(self, variable_set):
        """ Subset the set of variables (on place).

        :param variable_set: array of variables to select
        :type variable_set: numpy array
        """
        self.__values = np.ascontiguousarray(self.__values[:, variable_set])

    def to_json(self):
        """
        Convert the object to a json readable object
        """
        return dict(name=self.__name, values=self.__values.tolist())