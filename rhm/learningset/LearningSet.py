import numpy as np
import sys


class LearningSet(object):

    """
    Holds the learning set, ie both the series and the labels,
    if in a supervised setting.
    Data are stored as numpy arrays.
    """

    def __init__(self, series=None, labels=None, variables=None):

        """
        Initializes the learning set.
        :param series: `timeseries.TimeSeries` to use (default None)
        :type series: list or numpy array
        :param labels: the labels (default None)
        :type labels: list or numpy array
        """

        self.__series = series
        self.__label = labels
        self._variables = variables

    def __getitem__(self, idx):
        # returns the series with index id[x]
        return self.__series[idx]

    def __len__(self):
        # returns the number of series in the set
        return len(self.__series)

    def __repr__(self):
        # return a string representation of the learning set
        return "Series: {}\nLabels: {}".format(self.__series, self.__label)

    def append(self, other):

        """
         Merge the other learning set in self.

        :param other: the other learning set to append
        :type other: `LIG.LearningSet`
        """

        self.__series = np.concatenate((self.__series, other.get_series()))
        self.__label = np.concatenate((self.__label, other.get_labels()))

    def get_series(self):
        # returns the series (ie an array of TimeSeries)
        return self.__series

    def get_label(self, idx):
        # returns the label of serie with index idx
        return self.__label[idx]

    def set_label(self, idx, value):
        # Used in special cases
        self.__label[idx] = value

    def get_labels(self):
        # return the label set
        return self.__label

    @property
    def variables(self):
        return self._variables

    def time_len(self, idx):
        # returns the length of time series with index idx
        return self.__series[idx].time_len()

    def whole_interval(self):
        # return the whole time interval

        return 0, self.__series[0].time_len()

    def set_labels(self, labels):
        # setter for the labels
        self.__label = labels
        return self

    def cut_labels(self, labels):

        """
        remove the instances that don't belong to any of the labels
        :param labels: labels to consider
        """

        good_index = set()
        for i in range(len(self.__label)):
            if self.__label[i] in labels:
                good_index.add(i)

        self.__label = [i for j, i in enumerate(self.__label) if j in good_index]
        self.__series = [i for j, i in enumerate(self.__series) if j in good_index]

    def set_time_len(self, interval):
        """
        reduce each time series in the given interval

        :param interval: the interval (type tuple: [start, end) )
        """

        for _ts in self.__series:
            _ts.set_time_len(interval)

    def nb_variables(self):

        # return the number of variables of the timeseries

        return self.__series[0].nb_variables()

    def subset_variable(self, set_variable):

        """
        subset the variable set (on place)

        :param set_variable: array of variable indexes to keep
        """

        for _ts in self.__series:
            _ts.subset_variables(set_variable)

    def filter_variable(self, variable, value, ls=True):

        """
        filter variable values based on input value

        :param variable: variable of interest
        :param value: value to cut based upon
        :param ls: less or equal flag
        """

        if ls:
            for _ts in self.__series:
                _ts.filter_variable_values_ls(variable, value)
        else:
            for _ts in self.__series:
                _ts.filter_variable_values_gr(variable, value)

    def series_avg_length(self):

        """
        Calculate the average length of the whole dataset (average number of events)
        :return: the average value as float
        """

        _sum = sum([_ts.time_len() for _ts in self.__series])
        return float(_sum) / float(len(self.__series))

    def series_min_length(self):

        """
        Calculate the minimum length of the whole dataset (average number of events)
        :return: the minimum value as int
        """

        _min = sys.float_info.max
        for _ts in self.__series:
            if _ts.time_len() < _min:
                _min = _ts.time_len()
        return _min

    def series_max_length(self):

        """
        Calculate the maximum length of the whole dataset (average number of events)
        :return: the maximum value as int
        """

        _max = sys.float_info.min
        for _ts in self.__series:
            if _ts.time_len() > _max:
                _max = _ts.time_len()
        return _max

    def to_json(self):

        """
        convert the data to a json parsable object.
        :return: dictionary representation of the object
        """
        
        return dict(labels=self.__label, series=self.__series.tolist())
