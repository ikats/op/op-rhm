import numpy as np


class LearningSetHistogram(object):

    def __init__(self, data, nbins):
        self._data = data
        self._nbins = nbins
        self._histogram = None
        self._stats = None

    @property
    def nbins(self):
        return self._nbins

    @nbins.setter
    def nbins(self, value):
        self._nbins = value

    @property
    def stats(self):
        return self._stats

    def histogram(self, variable=0):
        self._histogram = []
        self._stats = dict()
        _index = 0
        _range = range(self._nbins + 1)
        for ts in self._data.get_series():
            _data = ts.data()
            _temp = [row[variable] for row in _data]
            self._histogram.append(np.histogram(_temp, _range)[0])
            _label = self._data.get_label(_index)
            if _label not in self._stats:
                self._stats[_label] = [0] * self._nbins
            self._stats[_label] = [sum(x) for x in zip(self._histogram[_index], self._stats[_label])]
            _index += 1

    def propose_nbins(self, variable=0):
        _set = set()
        _all_bins = 0
        for ts in self._data.get_series():
            _data = ts.data()
            _temp = [row[variable][0] for row in _data]
            #print(_temp)
            _all_bins += len(_temp)
            _set.update(set(_temp))
        _prop_bins = len(_set)
        return (_all_bins, _prop_bins)