import os
import numpy as np
import codecs
import json
from rhm.timeseries import TimeSeries_spark as ts
from rhm.learningset import LearningSet as ls
from ikatsbackend.core.resource.client import TemporalDataMgr


class LearningSetBuilder(object):
    """ provides different ways of loading a learning set """

    def __init__(self):
        """ clean start. Random state is initialize with number=50, time_len=100, nb_labels=4 """
        self.__input_shape = {"comments" : "#", 'delimiter' : ','}
        self.__known_format = {"json": self.build_from_json, "ucr": self.build_from_ucr, 'ikats': self.build_from_ikats,
                               'ucrd':self.build_from_ucr_wd}
        self.__builder = None
        # parameters for random generation
        self._random_config = {"number":50, "time_len" : 100, "nb_labels" : 4, "nb_variables":2}

    def set_input_format(self, in_format):

        """
        Defines the format that will be used.

        Available formats are:
           "ldf"
                Stands for "label dir, files": learning set is a set of directories, one per label,
                 each directory name being the name of the corresponding label.
                 In each directory we have a set of files, one per time series
                 LIMITATION: files system are not very good at handling huge set of files in
                 given directory.

        """

        if in_format not in self.__known_format:
            raise ValueError("format {} not handle (yet ?). Known formats are {}".
                             format(in_format, self.__known_format))
        self.__builder = self.__known_format[in_format]

    def set_text_shape(self, loadtxt_params, update=True):

        """Defines the parameters used when input format is a text  file.

        :param loadtxt_params: defines the input format as a dictionnary that represents the
                         parameters of the np.loadtxt() function.
        :param update: True if the current input_shape has to be updated, False if it has to be
                     replaced
        :type loadtxt_params: dictionnary
        :type update: Boolean (default to True)
        """

        if update:
            self.__input_shape.update(loadtxt_params)
        else:
            self.__input_shape = loadtxt_params.copy()

    def set_random_param(self, random_info):

        """
        :param random_info: the informations for the random generation.
        :type number: dictionnary
        """

        self._random_config = random_info

    def build(self, root_dir=None, nb_derivative=None, delimeter=None):
        """build and return a learningSet given the context """

        return self.__builder(root_dir, nb_derivative, delimeter)


    def build_from_ikats(self, input, nb_derivative=None, delimeter=None):
        _tdm = TemporalDataMgr()
        _input = []
        _series = []
        for _ts in input:
            _input.append(_ts['tsuid'])

        _labels = []
        md = _tdm.get_meta_data(_input)
        all_variables = set()
        for _ts in _input:
            all_variables.add(md[_ts]['metric'])

        series_dict = dict()
        _labels_dict = dict()
        for _ts in _input:
            obs_id = md[_ts]['obsId']
            var_id = md[_ts]['metric']
            if obs_id in series_dict:
                series_dict[obs_id][var_id] = _tdm.get_ts(tsuid_list=[_ts])[0][:, 1]
            else:
                series_dict[obs_id] = dict()
                _data = _tdm.get_ts(tsuid_list=[_ts])[0]
                series_dict[obs_id]['timestamp'] = _data[:, 0]
                series_dict[obs_id][var_id] = _data[:, 1]
                _labels_dict[obs_id] = md[_ts]['class']

        for obs_id, _data in series_dict.items():
            data = []
            data.append(_data['timestamp'])
            for var in sorted(all_variables):
                data.append(_data[var])
            data = np.array(data).T
            _label = _labels_dict[obs_id]
            _series.append(ts.TimeSeries(values=data, label=_label, id=obs_id))
            _labels.append(_labels_dict[obs_id])

        _series = np.array(_series)
        return ls.LearningSet(_series, _labels)

    def build_from_json(self, input_dir, nb_derivative=None, delimeter=None):
        """
        Build a learningset from a json file
        :param input_dir: The json file path
        :return: LearningSet
        """
        obj_text = codecs.open(input_dir, 'r', encoding='utf-8').read()
        _jsonData = json.loads(obj_text)
        _series = []
        _labels = []
        id = 0
        for value, label in zip(_jsonData['series'], _jsonData['labels']):
            _series.append(ts.TimeSeries(values=np.array(value['values']), name=value['name'], label= label, id=id))
            _labels.append(label)
            id += 1
        _series = np.array(_series)
        return ls.LearningSet(_series, _labels)

    def build_from_ucr(self, input_dir, nb_derivative=None, delimeter=None):
        """
        Build a learning set from a ucr data file (each row a ts)
        and calculate on the fly the nb_derivative derivatives.
        :param input_dir: Path to file
        :param nb_derivative: number of derivatives to calculate
        :param delimeter:
        :return: LearningSet
        """
        if not os.path.exists(input_dir):
            raise IOError("No such file or directory ({})".format(input_dir))
        _series = []
        _labels = []
        with open(input_dir, 'r') as _file:
            id = 0
            for line in _file:
                _arr = line.split(delimeter)
                if len(_arr) > 1:
                    label = str(_arr[0])
                    _labels.append(label)
                    _series.append(ts.TimeSeries(values=np.array([float(x) for x in _arr[1:]]), label=label, id=id))
                    id += 1
        _series = np.array(_series)
        return ls.LearningSet(_series, _labels)

    def build_from_ucr_wd(self, input_dir, nb_derivative=0, delimeter=None):
        """
        Build a learningset from a json file
        :param input_dir: The json file path
        :return: LearningSet
        """
        if not os.path.exists(input_dir):
            raise IOError("No such file or directory ({})".format(input_dir))
        _series = []
        _labels = []
        with open(input_dir, 'r') as _file:
            id = 0
            for line in _file:
                _arr = line.split(delimeter)
                _ts = []
                if len(_arr) > 1:
                    label = str(_arr[0])
                    _labels.append(label)
                    _ts.append([float(x) for x in _arr[1:]])
                    for i in range(nb_derivative):
                        _digits = 4 - i
                        _temp1 = _ts[i][:-1]
                        _temp2 = _ts[i][1:]
                        _temp_array = [round((_temp2[i] - _temp1[i]), _digits) for i in range(len(_temp1))]
                        _temp_array.append(0)
                        _ts.append(_temp_array)
                    _series.append(ts.TimeSeries(values=_ts, label=label, id=id))
                    id += 1
        _series = np.array(_series)
        return ls.LearningSet(_series, _labels)
