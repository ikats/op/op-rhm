import numpy as np
import logging
from rhm.timeseries import TimeSeriesBuilder as Ts
from rhm.learningset import LearningSet as Ls
from ikatsbackend.core.resource.api import IkatsApi as Api


logger = logging.getLogger(__name__)


class LearningSetBuilder(object):
    """ provides different ways of loading a learning set """

    def __init__(self):

        """ clean start. Random state is initialize with number=50, time_len=100, nb_labels=4 """

        self.__input_shape = {"comments": "#", 'delimiter': ','}
        self.__known_format = {'ikats': self.build_from_ikats}
        self.__builder = None
        # parameters for random generation
        self._random_config = {"number": 50, "time_len": 100, "nb_labels": 4, "nb_variables": 2}

    def set_input_format(self, in_format):

        """
        Defines the format that will be used.

        Available formats are:
           "ldf"
                Stands for "label dir, files": learning set is a set of directories, one per label,
                 each directory name being the name of the corresponding label.
                 In each directory we have a set of files, one per time series
                 LIMITATION: files system are not very good at handling huge set of files in
                 given directory.

        """

        if in_format not in self.__known_format:
            raise ValueError("format {} not handle (yet ?). Known formats are {}".
                             format(in_format, self.__known_format))
        self.__builder = self.__known_format[in_format]

    def set_text_shape(self, loadtxt_params, update=True):

        """
        Defines the parameters used when input format is a text  file.

        :param loadtxt_params: defines the input format as a dictionary that represents the
                         parameters of the np.loadtxt() function.
        :param update: True if the current input_shape has to be updated, False if it has to be
                     replaced
        :type loadtxt_params: dictionnary
        :type update: Boolean (default to True)
        """

        if update:
            self.__input_shape.update(loadtxt_params)
        else:
            self.__input_shape = loadtxt_params.copy()

    def build(self, root_dir=None, nb_derivative=None, delimeter=None):

        """build and return a learningSet given the context"""
        return self.__builder(root_dir, nb_derivative, delimeter)

    def build_from_ikats(self, ls_table_name, id_label='flight_id', id_class='target'):

        """
        Extract information from a table and format the output as a dict of dict
        The first key will be the obs_id values taken from the table_content.

        :param ls_table: the JSON content corresponding to the table as dict
        :param id_label: Column name used as primary key
        :param id_class: Column name used as target key

        :type ls_table: dict
        :type id_label: str
        :type id_class: str

        :return: a dict of dict where first key is the obs_id and the sub keys are the items
        :rtype: dict
        """

        # 2D array containing the equivalent of the rendered JSON structure
        data_array = []
        variables = []
        columns_name = dict()

        ls_table = Api.table.read(ls_table_name)
        #logger.info('read table ---- %s %s' % (ls_table_name, ls_table))

        # obj_text = codecs.open(ls_table, 'r', encoding='utf-8').read()
        # obj_text = codecs.open('/home/ikats/src/Airbus_1_new.json', 'r', encoding='utf-8').read()
        # json_data = json.loads(ls_table)
        try:
            # Get the columns name with a mapping dict
            for v, k in enumerate(ls_table["headers"]["col"]["data"]):
                columns_name[k] = v
                variables.append(k)
        except:
            raise ValueError("Table content shall contain col headers to know the name of columns")

        """
        if id_label not in columns_name or id_class not in columns_name:
            raise ValueError("Table header should contain a unique ID and a target column")
        """

        try:
            # Fill the 2D array with the content of the header column
            # Skip the first cell by starting at index 1
            data_array = list(map(lambda x: [x], ls_table["headers"]["row"]["data"][1:]))
        except KeyError:
            # No header column present, skip it
            pass

        ts_builder = Ts.TimeSeriesBuilder()
        series = []
        labels = []
        items = variables[1:len(variables) - 1]
        id_class = len(variables) - 1

        for line_index, line in enumerate(ls_table["content"]["cells"]):

            if len(data_array) < line_index:
                # Fill in the data_array line with an empty list in case there was no header column
                data_array.append([])
            data_array[line_index].extend(line)
            ts_data = None
            null_check = False
            for item in items:
                item_ind = columns_name[item]
                func_id = data_array[line_index][item_ind]
                # tsuid = api.fid.tsuid(func_id)
                tsuid = None
                for link in ls_table['content']['links'][line_index]:
                    if link:
                        if func_id == link['val'][0]['funcId']:
                            tsuid = link['val'][0]['tsuid']
                            break
                        else:
                            continue

                if tsuid is None:
                    null_check = True
                    break
                temp_ts_data = Api.ts.read(tsuid_list=[tsuid])[0]
                if ts_data is None:
                    ts_data = [temp_ts_data[:, 0]]
                ts_data.append(temp_ts_data[:, 1])
            if null_check:
                continue
            time_series = ts_builder.build_from_array(np.array(ts_data).T)
            time_series.id = data_array[line_index][0]
            series.append(time_series)
            labels.append(str(data_array[line_index][id_class]))

        _series = np.array(series)
        items = ['timestamp'] + items
        return Ls.LearningSet(_series, labels, items)
